
    <!--****************************************************** 3ra sessao  ******************************************************************-->        

  <section>
      <div class="container-fluid">

        <div class="row" style="margin-top: 80px;">
          
          <div class="col-xs-12 nopadding">
            <div class="sec-title-container text-center">
              <p class="by-sub-title font-size-text wow animated fadeInUpBig"><?=home_three_text_1?> </p>
              <h4 class="uppercase font-weight-7 less-mar-1 font-size-title wow animated fadeInUpBig" style="margin-bottom: 10px;"><?=home_three_text_2?></h4>
              <div class="ce4-title-line-1 wow animated fadeInLeft" style="background-color: #f50f40;"></div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="clearfix"></div>
          
        </div>

        <div class="row no-gutter">

        <?php
          echo funGetSlide('home_three','','','

            <div class="col-md-4">
             <div class="ce4-feature-box-46 {{subtitle}}">
               <div class="img-box">
                 <div class="{{ctaTitle}}"></div>
                 <div class="overlay">
                   <div class="text-box">
                     <a href="{{callAction}}"><h4 class="text-white less-mar-1 wow animated fadeInUp" data-wow-delay="0.4s">{{title}}</h4></a>
                     <p class="text-white font-size-text wow animated fadeInUp" data-wow-delay="0.4s">{{text}}</p>
                   </div>
                 </div>
                 <img src="{{img}}" alt="" class="img-responsive"/>
                </div>
             </div>
           </div>
            
          ');
        ?>  

        <!--   <div class="col-md-4">
             <div class="ce4-feature-box-46 mar-top">
               <div class="img-box">
                 <div class="overlay">
                   <div class="text-box">
                     <h4 class="text-white less-mar-1">IMPLANTES DENTÁRIOS </h4>
                     <p class="text-white">Um tratamento definitivo para um novo sorriso.</p>
                   </div>
                 </div>
                 <img src="<?php /*base_url('imagem_4.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/>
                </div>
             </div>
           </div>
           
           
           <div class="col-md-4">
             <div class="ce4-feature-box-46 middle-item">
               <div class="img-box">
                 <div class="border-big"></div>
                 <div class="overlay">
                   <div class="text-box">
                     <a href="#avaliacao" ><h4 class="text-white less-mar-1">REABILITAÇÃO ORAL</h4></a>
                     <p class="text-white">A solução mais completa para o seu sorriso.</p>
                   </div>
                 </div>
                 <img src="<?php /*base_url('imagem_5.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/>
               </div>
             </div>
           </div>
           
           
          <div class="col-md-4">
           <div class="ce4-feature-box-46 mar-top">
             <div class="img-box">
               <div class="overlay">
                 <div class="text-box">
                   <h4 class="text-white less-mar-1">TID®</h4>
                   <p class="text-white">A opção com o melhor custo-benefício para si.</p>
                 </div>
               </div>
               <img src="<?php /*base_url('imagem_6.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/>
             </div>
           </div>
          </div>
            -->

        </div>
      </div>
  </section>
  <div class="clearfix"></div>