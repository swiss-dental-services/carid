<!--****************************************************** 1ra sessao  **********************************************************************-->


<section class="sec-tpadding-2">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 nopadding">
        <div class="ce4-feature-box-1- ce4-feature-box-1-custon-1 margin-bottom" style="/*height: 842px;*/">
           <div class="img-box-main"> 
             <div class="img-box">
               <img src="<?php base_url('imagem_2.png','img/custon') ?>" alt="" class="img-responsive"/> 
             </div>

             <div class="clearfix"></div>
             <br/>
             <div class="text text-custon wow animated fadeInUp">
                <p class="font-size-text"><?=home_one_text_1?></p>
             </div>           

           </div>

          </div>
      </div>
      <!--end item-->
      
      <div class="col-md-6 margin-bottom text-center nopadding">
      
      
        <div class="ce4-feature-box-48 height-custon" style="/*height: 711px;*/">
        
          <div class="text-box-main">
            <div class="text-box">
              <div class="col-xs-12 nopadding">
                <div class="sec-title-container text-center">
                  <br/><br/>
                  <h4 class="ce4-big-title uppercase font-weight-b mobil-title-1 wow animated fadeInUp"><?=home_one_text_2?> <br><?=home_one_text_3?> <br><?=home_one_text_4?></h4>
                  
                </div>
              </div>
              <div class="clearfix"></div>
              <!--end title-->     
              <?php
                echo funGetSlide('home_one','','','
                  
                <h4 class="raleway wow animated fadeInUp">{{subtitle}} </h4>
                <br/>
                <p class="font-size-text wow animated fadeInUp">{{text}}</p>
                ');
              ?>                       
                 
              <div class="clearfix"></div>

              <div style="padding-top: 50px;" class="wow animated fadeInUp">
                <a href="casos-clinicos" class="ce4-readmore-big- btn-2 btn-red btn-2-border">Saber mais</a>
              </div>
              

            </div>
          </div>
        </div>
        <!--end right box-->

      </div>
      <!--end item--> 
      
    </div>
  </div>
</section>













        
    