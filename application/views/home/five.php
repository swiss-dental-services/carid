
    <!--****************************************************** 5ta sessao  ***************************************************************-->  



<section class="sec-padding">
      <div class="container">
        <div class="row">
          <div class="background-imgholder-full img3 mt-50-desk">
            <div class="col-md-6 col-md-offset-6-"> <br/>
              <br/>
              <div class="col-xs-12 nopadding">
                <div class="sec-title-container less-padding-3 text-left">
                  <div class="ce4-title-line-1 align-left wow animated fadeInLeft" style="background-color: #f50f40;"></div>
                  <h4 class="uppercase font-weight-7 less-mar-1 font-size-title wow animated fadeInRight"><?=home_five_text_1?></h4>
                </div>
              </div>
              <div class="clearfix"></div>
              <!--end title-->
              
              <p class="text-left font-size-text wow animated fadeInUp"><?=home_five_text_2?></p>
              <div class="clearfix"></div>
              <br/>
              <ul class="iconlist">
                <li class="font-size-text wow animated fadeInUp" data-wow-delay="0.0s"><i class="fa fa-arrow-circle-right" style="color: #f50f40;"></i> PNID — Programa Nacional de Implantes Dentários </li>
                <li class="font-size-text wow animated fadeInUp" data-wow-delay="0.3s"><i class="fa fa-arrow-circle-right" style="color: #f50f40;"></i> MEID — Movimento Europeu de Implantes Dentários </li>
                <li class="font-size-text wow animated fadeInUp" data-wow-delay="0.6s"><i class="fa fa-arrow-circle-right" style="color: #f50f40;"></i> Swiss Dental Health Plans </li>
                <li class="font-size-text wow animated fadeInUp" data-wow-delay="0.9s"><i class="fa fa-arrow-circle-right" style="color: #f50f40;"></i> Swiss Dental Services </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="row mt-60-mobile wow animated fadeInUp" style="padding-top: 50px; display: flex; justify-content: center;" >
          <a href="campanhas" class="smooth-scroll btn-2 btn-red btn-2-border">Conheça as campanhas</a>
        </div>

      </div>
    </section>
    <div class="clearfix"></div>