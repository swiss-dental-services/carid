   <!--****************************************************** 4ta sessao ************************************************************--> 
   
<section class="ce4-parallax-sec-2">
  <div class="section-overlay bg-opacity-7- bg-opacity-home">
    <div class="container sec-tpadding-3">
      <div class="row">
        
        <div class="col-md-6 OnlyDesktop wow animated fadeInUp" style="position: relative;">
            <div class="ce4-feature-box-47"  style="position: absolute; top: 80px;">
                <img src="<?php base_url('imagem_7.png','img/custon') ?>" alt="" class="img-responsive" style="max-width: 115%;"/>
            </div>
        </div>
        <!--end item-->
        
        <div class="col-md-6 text-left pb-70 pt-50-mobile">
            <div class="col-divider-margin-5"></div>
            <div class="col-xs-12 nopadding">
                <div class="sec-title-container text-left">                    
                  <h4 class="ce4-big-title text-white uppercase font-weight-b mobil-title-2 wow animated fadeInLeft"><?=home_four_text_1?> <br><?=home_four_text_2?> <br><?=home_four_text_3?><span  style="color: #f50f40;">.</span> </h4>
                </div>
            </div>
            <div class="clearfix"></div>
                  <!--end title-->
            <?php
              echo funGetSlide('home_four','','','

                <h4 class="raleway text-light wow animated fadeInRight" style="color: #fff;">{{subtitle}} </h4>
                <p class="font-size-text wow animated fadeInLeft" style="color: #fff; margin-bottom: 35px;">{{text}}</p>                 
                
              ');
            ?>       
                  
            <div class="clearfix"></div> 
            <div class="wow animated fadeInUp">
              <a href="casos-clinicos" class="ce4-readmore-big- less-pad btn-2 btn-red">Saber mais</a>              
            </div>
             
        </div>
        <!--end item-->
        
      </div>
    </div>
  </div>
</section>
<div class="clearfix"></div>