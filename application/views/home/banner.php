<!--================Slider Home Area =================-->
<!-- REVO SLIDER FULLSCREEN 1 -->
<?php
    
    $tablet  = false;
    $mobil   = false;
    $desktop = false;
    $data_y_tex_1_slide_1  = 0;
    $data_y_butom_slide_1  = 0;

    $data_y_butom_slide_2  = 0;

    $data_y_tex_1_slide_3  = 0;
    $data_y_butom_slide_3  = 0;

    $data_y_butom_slide_4  = 0;


    if(isTablet()){
        $tablet  = true;
    }
    if(isMobile()){
        $mobil   = true;
    }
    if($tablet == false && $mobil== false){
        $desktop = true;
    }

    if($tablet  == true){

        //echo "Olá, eu sou um tablet";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 495;

        $data_y_tex_1_slide_2  = 0;
        $data_y_butom_slide_2  = 460;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 495;

        $data_y_butom_slide_4  = 460;

    }else if($mobil  == true){

        //echo "Olá, eu sou um mobil";
        $data_y_tex_1_slide_1  = 510;
        $data_y_butom_slide_1  = 650;

        $data_y_butom_slide_2  = 600;

        $data_y_tex_1_slide_3  = 510;
        $data_y_butom_slide_3  = 650;

        $data_y_butom_slide_4  = 600;

    }elseif ($desktop == true) {
        
        //echo "Olá, eu sou um computador";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 480;

        $data_y_butom_slide_2  = 450;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 480;

        $data_y_butom_slide_4  = 450;
    }


?>
<div class="clearfix"></div>
    
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="slide-tmargin">
      <div class="slidermaxwidth">
        <div class="rev_slider_wrapper"> 
          <!-- START REVOLUTION SLIDER 5.0 auto mode -->
          <div id="rev_slider" class="rev_slider"  data-version="5.0">
            <ul>
              
              <!-- SLIDE 1  -->
              <li data-index="rs-1" data-transition="fade"> 
                <!-- MAIN IMAGE --> 
                <img src="<?php base_url('banner_topo_1.jpg','img/custon') ?>" alt=""  width="1920" height="1280"> 
                
                <!-- LAYER NR. 1 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
            id="slide-1-layer-1" 
            data-x="['left','left','center','center']" data-hoffset="['570','450','0','0']" 
            data-y="['top','top','top','bottom']" data-voffset="['130','30','390','0']" 
            data-width="none" data-height="none" 
            data-whitespace="nowrap" 
            data-transform_idle="o:1;" 
            data-transform_in="x:right;s:1500;e:Power3.easeOut;" 
            data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" 
            data-start="2750" 
            data-responsive_offset="on" 
            style="z-index: 5;text-transform:left;"> <img src="<?php base_url('imagem_banner_1.png','img/custon') ?>" alt="" width="725" height="805" data-ww="['725','725','725','546']" data-hh="['805','805','805','606']" data-no-retina> </div>
                
            <!-- LAYER NR. 2 -->
            <div class="tp-caption roboto bold uppercase white tp-resizeme banner-title-custon"
            id="slide-1-layer-2" 
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['340','200','80','80']" 
            data-fontsize="['80','80','50','30']"
            data-lineheight="['70','70','70','50']"
            data-transform_idle="o:1;tO:0% 50%;"
            data-transform_in="x:-50px;rY:-90deg;opacity:0;s:2000;e:Back.easeOut;" 
            data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-start="1000" 
            data-splitin="lines" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.05" 
            style="z-index: 6; white-space: nowrap; font-size: 10px; line-height: 30px;"><?=home_banner_slide_1_text_1?></div>
                
            <!-- LAYER NR. 3 
            <div class="tp-caption raleway font-weight-2 uppercase white tp-resizeme"
            id="slide-1-layer-3" 
            data-x="['left','left','center','center']" data-hoffset="['330','330','110','70']" 
            data-y="['top','top','top','top']" data-voffset="['340','200','80','80']" 
            data-fontsize="['80','80','50','30']"
            data-lineheight="['70','70','70','50']"
            data-transform_idle="o:1;tO:0% 50%;"
            data-transform_in="x:-50px;rY:-90deg;opacity:0;s:2000;e:Back.easeOut;" 
            data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-start="1000" 
            data-splitin="lines" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.05" 
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px;">&</div>-->
                
            <!-- LAYER NR. 4 -->
            <div class="tp-caption roboto bold uppercase white tp-resizeme"
            id="slide-1-layer-4"
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['400','260','140','110']" 
            data-fontsize="['80','80','50','30']"
            data-lineheight="['70','70','70','50']"
            data-transform_idle="o:1;tO:0% 50%;"
            data-transform_in="x:-50px;rY:-90deg;opacity:0;s:2000;e:Back.easeOut;" 
            data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-start="1500" 
            data-splitin="lines" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.05" 
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px;"><?=home_banner_slide_1_text_2?></div>
                
                <!-- LAYER NR. 5 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
            id="slide-1-layer-5" 
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['480','340','210','170']" 
            data-width="300"
            data-height="2"
            data-whitespace="nowrap"
            data-transform_idle="o:1;"
            data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
            data-transform_out="opacity:0;s:300;s:300;" 
            data-mask_in="x:0px;y:0px;" 
            data-start="2000" 
            data-responsive_offset="on" 
            style="z-index: 6;text-transform:left;background-color:#fff;border-color:rgba(0, 0, 0, 0);"> </div>
                
                <!-- LAYER NR. 6 -->
                <div class="tp-caption roboto dark-grey uppercase white tp-resizeme"
            id="slide-1-layer-6"
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['480','340','220','180']" 
            data-fontsize="['16','16','14','14']"
            data-lineheight="['70','70','70','50']"
            data-transform_idle="o:1;tO:0% 50%;"
            data-transform_in="x:-50px;rY:-90deg;opacity:0;s:2000;e:Back.easeOut;" 
            data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-start="2500" 
            data-splitin="lines" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.05" 
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px; color: #fff;"><?=home_banner_slide_1_text_3?></div>
                
                <!-- LAYER NR. 7 -->
                <div class="tp-caption sbut1- cta-banner"
            id="slide-1-layer-7"
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['580','440','340','280']"
            data-speed="800"
            data-start="3000"
            data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
            data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;" 
            data-endspeed="300"
            data-captionhidden="off"
            style="z-index: 6"> <a href="#avaliacao" class="smooth-scroll">Marcar avaliação</a> </div>
              </li>
              
              <!-- SLIDE 2 -->
              <li data-index="rs-2" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="" data-description=""> 
                
                <!-- MAIN IMAGE --> 
                <img src="<?php base_url('banner_topo_2.jpg','img/custon') ?>" alt=""  data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina> 
                
                <!-- LAYER NR. 1 -->
                <div class="tp-caption raleway fweight-2 white uppercase tp-resizeme rs-parallaxlevel-0 banner-title-custon"
            id="slide-2-layer-1" 
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['340','200','80','80']" 
            data-fontsize="['80','80','50','30']"
            data-lineheight="['70','70','70','50']"
            data-width="none"
            data-height="none"
            data-whitespace="nowrap"
            data-transform_idle="o:1;"           
            data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
            data-start="1000" 
            data-splitin="chars" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.1"                         
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px;"><?=home_banner_slide_2_text_1?></div>
                
                <!-- LAYER NR. 2 -->
                <div class="tp-caption raleway fweight-4 white uppercase tp-resizeme rs-parallaxlevel-0"
            id="slide-2-layer-2" 
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['400','260','140','110']" 
            data-fontsize="['80','80','50','30']"
            data-lineheight="['70','70','70','50']"
            data-width="none"
            data-height="none"
            data-whitespace="nowrap"
            data-transform_idle="o:1;"           
            data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
            data-start="1500" 
            data-splitin="chars" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.1"                         
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px;"><?=home_banner_slide_2_text_2?></div>
                
                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
            id="slide-2-layer-3" 
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['480','340','210','170']" 
            data-width="300"
            data-height="2"
            data-whitespace="nowrap"
            data-transform_idle="o:1;"
            data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
            data-transform_out="opacity:0;s:300;s:300;" 
            data-mask_in="x:0px;y:0px;" 
            data-start="3000" 
            data-responsive_offset="on" 
            style="z-index: 6;text-transform:left;background-color:#fff;border-color:rgba(0, 0, 0, 0);"> </div>
                
                <!-- LAYER NR. 4 -->
                <div class="tp-caption roboto dark-grey uppercase white tp-resizeme"
            id="slide-2-layer-4"
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['480','340','220','180']" 
            data-fontsize="['16','16','14','14']"
            data-lineheight="['70','70','70','50']"
            data-transform_idle="o:1;tO:0% 50%;"
            data-transform_in="x:-50px;rY:-90deg;opacity:0;s:2000;e:Back.easeOut;" 
            data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
            data-start="2500" 
            data-splitin="lines" 
            data-splitout="none" 
            data-responsive_offset="on" 
            data-elementdelay="0.05" 
            style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 30px; color: #fff;"><?=home_banner_slide_2_text_3?></div>
                
                <!-- LAYER NR. 5 -->
                <div class="tp-caption sbut1- cta-banner"
            id="slide-2-layer-5"
            data-x="['left','left','center','center']" data-hoffset="['50','50','0','0']" 
            data-y="['top','top','top','top']" data-voffset="['580','440','340','280']"
            data-speed="800"
            data-start="3000"
            data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
            data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;" 
            data-endspeed="300"
            data-captionhidden="off"
            style="z-index: 6"> <a href="#avaliacao" class="smooth-scroll">Marcar avaliação</a> </div>
              </li>           
                           
            </ul>
          </div>
          <!-- END REVOLUTION SLIDER --> 
        </div>
      </div>
      <!-- END REVOLUTION SLIDER WRAPPER --> 
    </div>
    <div class="clearfix"></div>