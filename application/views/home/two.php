<!--****************************************************** 2da sessao  *************************************************************-->


<section class="sec-padding section-light">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 nopadding">
        <div class="sec-title-container text-center">
          <p class="by-sub-title font-size-text wow animated fadeInUpBig"><?=home_two_text_1?> </p>
          <h4 class="uppercase font-weight-7 less-mar-1 font-size-title wow animated fadeInUpBig" style="margin-bottom: 10px;"><?=home_two_text_2?></h4>
          <div class="ce4-title-line-1 wow animated fadeInLeft"></div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <!--end title-->


      <?php
        echo funGetSlide('home_two','','','

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="ce4-feature-box-3 margin-bottom wow animated fadeInUp" data-wow-delay="{{ctaTitle}}">
              <div class="inner-box text-center">
                <div class="iconbox-small center outline-gray-2 round"><img src="{{img}}" alt="" style="width: 70%;"></div>
                <h5 class="uppercase less-mar-1">{{title}}</h5>
                <div class="title-line" style="background-color: #f50f40;"></div>
                <br/>
                <p>{{text}}</p>
              </div>
            </div>
          </div>

          
          ');
      ?> 
        
      
    </div>

    <div class="row wow animated fadeInUp" style="padding-top: 50px; display: flex; justify-content: center;" >
      <a href="#avaliacao" class="smooth-scroll btn-2 btn-red btn-2-border">Marcar avaliação</a>
    </div>

  </div>
</section>

<div class="clearfix"></div>