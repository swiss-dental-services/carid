<!--================Slider Home Area =================-->
<!-- REVO SLIDER FULLSCREEN 1 -->
<?php
    
    $tablet  = false;
    $mobil   = false;
    $desktop = false;
    $data_y_tex_1_slide_1  = 0;
    $data_y_butom_slide_1  = 0;

    $data_y_butom_slide_2  = 0;

    $data_y_tex_1_slide_3  = 0;
    $data_y_butom_slide_3  = 0;

    $data_y_butom_slide_4  = 0;


    if(isTablet()){
        $tablet  = true;
    }
    if(isMobile()){
        $mobil   = true;
    }
    if($tablet == false && $mobil== false){
        $desktop = true;
    }

    if($tablet  == true){

        //echo "Olá, eu sou um tablet";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 495;

        $data_y_tex_1_slide_2  = 0;
        $data_y_butom_slide_2  = 460;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 495;

        $data_y_butom_slide_4  = 460;

    }else if($mobil  == true){

        //echo "Olá, eu sou um mobil";
        $data_y_tex_1_slide_1  = 510;
        $data_y_butom_slide_1  = 650;

        $data_y_butom_slide_2  = 600;

        $data_y_tex_1_slide_3  = 510;
        $data_y_butom_slide_3  = 650;

        $data_y_butom_slide_4  = 600;

    }elseif ($desktop == true) {
        
        //echo "Olá, eu sou um computador";
        $data_y_tex_1_slide_1  = 485;
        $data_y_butom_slide_1  = 480;

        $data_y_butom_slide_2  = 450;

        $data_y_tex_1_slide_3  = 485;
        $data_y_butom_slide_3  = 480;

        $data_y_butom_slide_4  = 450;
    }


?>


<div class="header-inner-tmargin">
    <?php
        echo funGetAdvancedBanners('banner_sobre', '

    <section class="section-side-image clearfix">
      <div class="img-holder col-md-12 col-sm-12 col-xs-12">
        <div class="background-imgholder" style="background:url({{img}}); background-position: 50% 25% !important;"><img class="nodisplay-image" src="" alt=""/> </div>
      </div>
      <div class="container-fluid" >
        <div class="row">       
        <div class="col-md-12 col-sm-12 col-xs-12 clearfix nopadding">
        <div class="header-inner"><div class="overlay">       
      <div class="text text-center">
        <h3 class="uppercase text-white less-mar-1 title wow animated fadeInUpBig" data-wow-delay="0.5s">{{title}}</h3>
        <h6 class="uppercase text-white sub-title wow animated fadeInUpBig" data-wow-delay="0.8s">{{subtitle}}</h6>
      </div>
       </div>       
        </div></div>        
        </div>
      </div>
    </section>
                                                     
            ');
    ?>
    <div class=" clearfix"></div>
    <!--end section-->
</div>
<div class="clearfix"></div>
    <!--end header inner-->