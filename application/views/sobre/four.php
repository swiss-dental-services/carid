   <!--****************************************************** 4ta sessao ************************************************************--> 
   


   <section class="section-side-image clearfix">
      <div class="img-holder col-md-12 col-sm-12">
        <div class="background-imgholder" style="background:url(<?php base_url('imagem_3.jpg','img/custon') ?>);"><img class="nodisplay-image" src="http://placehold.it/1500x960" alt=""/> </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-md-offset-5 col-sm-8 col-sm-offset-4 text-inner-4 clearfix align-left">
            <div class="text-box white-transparent-1 padding-7">
              <div class="col-xs-12 nopadding">
                <div class="sec-title-container less-padding-3 text-left">
                  <div class="ce4-title-line-1 align-left wow animated fadeInUp" style="background-color: #f50f40;"></div>
                  <h4 class="uppercase font-weight-7 less-mar-1 font-size-title wow animated fadeInUp"><?=sobre_four_text_1?></h4>
                  <div class="clearfix"></div>
                  <p class="by-sub-title font-size-text wow animated fadeInUp"><?=sobre_four_text_2?></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <!--end title-->
              
              <div class="ce4-feature-box-38 wow animated fadeInUp" data-wow-delay="0.0s">
                <div class="iconbox-smedium round left icon" style="position: relative;">
                  <img class="hide1" src="<?php base_url('icons_sobre-01-vermelho.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">
                  <img class="top" src="<?php base_url('icons_sobre-01.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">
                </div>
                <?php
                  echo funGetSlide('sobre_four_1','','','

                <div class="text-box-right more-padding-1 div-reajuste">
                  <h5 class="title uppercase hove-title-red"><a href="reabilitacao-oral#ROTA-RO">{{title}}</a></h5>
                  <p class="text-reajust font-size-text">{{text}}</p>
                </div>

                    ');
                ?>
              </div>
              <!--end feature box-->
              
              <div class="col-divider-margin-3"></div>
              <div class="ce4-feature-box-38 active- wow animated fadeInUp" data-wow-delay="0.3s">
                <div class="iconbox-smedium round left icon" style="position: relative;">
                  <img class="hide1" src="<?php base_url('icons_sobre-02-vermelho.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">
                  <img class="top" src="<?php base_url('icons_sobre-02.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">  
                </div>
                <?php
                  echo funGetSlide('sobre_four_2','','','
                    
                <div class="text-box-right more-padding-1 div-reajuste">
                  <h5 class="title uppercase hove-title-red" style="/*color: #f50f40;*/"><a href="reabilitacao-oral#TID-RO">{{title}}</a> </h5>
                  <p class="text-reajust font-size-text">{{text}}</p>
                </div>

                    ');
                ?>
              </div>
              <!--end feature box-->
              
              <div class="col-divider-margin-3"></div>
              <div class="ce4-feature-box-38 wow animated fadeInUp" data-wow-delay="0.6s">
                <div class="iconbox-smedium round left icon" style="position: relative;">
                  <img class="hide1" src="<?php base_url('icons_sobre-03-vermelho.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">
                  <img class="top" src="<?php base_url('icons_sobre-03.svg','img/custon') ?>" alt="" style="width: 65%; position: absolute; left: 15px; top: 15px;">
                </div>
                <?php
                  echo funGetSlide('sobre_four_3','','','

                <div class="text-box-right more-padding-1 div-reajuste">
                  <h5 class="title uppercase hove-title-red"><a href="reabilitacao-oral#ID-RO">{{title}}</a></h5>
                  <p class="text-reajust-2 font-size-text">{{text}}</p>
                </div>

                    ');
                ?>
              </div>
              <!--end feature box--> 
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class=" clearfix"></div>
    <!--end section-->