
    <!--****************************************************** 5ta sessao  ***************************************************************-->  



    <section class="sec-tpadding-2 section-light mb-100-neg-mobile">
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-12 margin-bottom wow animated fadeInLeft"> <img src="<?php base_url('imagem_4_sobre.png','img/custon') ?>" alt="" class="img-responsive"/> </div>
          <!--end item-->
          
          <div class="col-md-7 col-sm-7 col-xs-12">
            <div class="col-xs-12 nopadding">
              <div class="sec-title-container less-padding-3 text-left">
                <h5 class="uppercase font-weight-4 nopadding wow animated fadeInUp"><?=sobre_five_text_1?></h5>
                <h2 class="uppercase font-weight-7 less-mar-1 wow animated fadeInUp"><?=sobre_five_text_2?><span class="text-gyellow" style="color: #f50f40;"><?=sobre_five_text_3?></span></h2>
                <!--<div class="ce4-title-line-1 align-left"></div>-->
              </div>
            </div>
            <div class="clearfix"></div>
            <!--end title-->
            <?php
              echo funGetSlide('sobre_five','','','

            <div class="ce4-feature-box-18 margin-bottom padding-mobile wow animated fadeInUp" style="background-color: transparent;">
              <h4 class="uppercase">{{title}}</h4>
              <h6 class="text-gyellow raleway font-size-text" style="color: #f50f40;">{{text}} </h6>
              <p class="font-size-text">{{subtitle}}</p>
              <br/>
              <a class="btn-2 btn-red btn-2-border uppercase" href="{{callAction}}">{{ctaTitle}}</a> 
            </div>                

                ');
                ?>

          </div>
          <!--end item--> 
        </div>
      </div>
    </section>
    <!--<div class="clearfix"></div>
     end section -->