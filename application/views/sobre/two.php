<!--****************************************************** 2da sessao  *************************************************************-->


  <section>
      <div class="container-fluid">
        <div class="row no-gutter">
          <?php
              echo funGetSlide('sobre_two_1','','','

          <div class="col-md-6">
            <div class="ce4-feature-box-35">
              <div class="img-box mobil-img-box-1">
                <div class="text-box mobil-text-box">
                  <div class="col-xs-12 nopadding">
                    <div class="sec-title-container less-padding-4 text-left">
                      <div class="ce4-title-line-1 align-left wow animated fadeInUp" style="background-color: #f50f40;"></div>
                      <h4 class="uppercase font-weight-7 less-mar-1 text-white wow animated fadeInUp">{{title}}</h4>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <!--end title-->
                  <p style="margin-bottom: 15px; color: #fff;" class="wow animated fadeInUp" >{{text}} </p>

                  <!--<a class="read-more wow animated fadeInUp" href="{{callAction}}" style="color: #f50f40;">{{ctaTitle}} &nbsp; &nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>-->

                </div>
                <img src="{{img}}" alt="" class="img-responsive OnlyDesktop"/> </div>
            </div>
          </div>

              ');
          ?>
          <!--end item-->
          
          <?php
            echo funGetSlide('sobre_two_2','','','

          <div class="col-md-6">
            <div class="ce4-feature-box-35">
              <div class="img-box mobil-img-box-2">
                <div class="text-box two mobil-text-box">
                  <div class="col-xs-12 nopadding">
                    <div class="sec-title-container less-padding-4 text-left">
                      <div class="ce4-title-line-1 align-left wow animated fadeInUp" style="background-color: #f50f40;"></div>
                      <h4 class="uppercase font-weight-7 less-mar-1 text-white wow animated fadeInUp">{{title}}</h4>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <!--end title-->
                  <p style="margin-bottom: 15px; color: #fff;" class="wow animated fadeInUp">{{text}} </p>

                  <!--<a class="read-more wow animated fadeInUp" href="{{callAction}}" style="color: #f50f40;">{{ctaTitle}} &nbsp; &nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>-->

                </div>
                <img src="{{img}}" alt="" class="img-responsive OnlyDesktop"/> </div>
            </div>
          </div>

            ');
          ?>
          <!--end item--> 
          
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->