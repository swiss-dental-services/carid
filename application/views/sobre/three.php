
    <!--****************************************************** 3ra sessao  ******************************************************************-->        

    <section>
      <div class="divider-line solid light"></div>
      <div class="container">
        <div class="row sec-padding">
          <div class="col-md-6 wow animated fadeInLeft">

            <div class="border-div">
              <a href="https://www.youtube.com/watch?v=ADy-Yd8g3po" class="v2 js-video-play"><img class="img-play" src="assets/img/custon/play-01.svg"></a> 
            </div> 
                                   
          </div>
          <!--end item-->
          
          <div class="col-md-6 padding-left-3">
            <div class="col-xs-12 nopadding">
              <div class="sec-title-container less-padding-3 text-left">
                <div class="ce4-title-line-1 align-left wow animated fadeInUp" style="background-color: #f50f40;"></div>
                <h4 class="uppercase font-weight-7 less-mar-1 wow animated fadeInUp"><?=sobre_three__text_1?> <span  style="color: #f50f40;"><?=sobre_three__text_2?></span>  <br> <span  style="color: #f50f40;"><?=sobre_three__text_3?></span> <?=sobre_three__text_4?> </h4>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="clearfix"></div>
            <!--end title-->
            <?php
              echo funGetAdvancedBanners('sobre_three', '

                <p class="font-size-text wow animated fadeInUp">{{text}}</p>
                <br/>
                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="0.0s">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow" style="color: #f50f40"></i></div>
                  <div class="text font-size-text">{{subtext}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="0.2s">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow" style="color: #f50f40"></i></div>
                  <div class="text font-size-text">{{title}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="0.4s">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow" style="color: #f50f40"></i></div>
                  <div class="text font-size-text">{{subtitle}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="0.6s">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow" style="color: #f50f40"></i></div>
                  <div class="text font-size-text">{{callTitle}}</div>
                </div>
                <!--end item--> 

                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="0.8s">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow" style="color: #f50f40"></i></div>
                  <div class="text font-size-text">{{callAction}}</div>
                </div>
                <!--end item--> 
                                      
              ');
            ?>
            
          </div>
          <!--end item--> 

         <div class="row wow animated fadeInUp" style="padding-top: 50px; display: flex; justify-content: center;" >
          <a href="casos-clinicos" class="smooth-scroll btn-2 btn-red btn-2-border">Conheça</a>
         </div>
         
        </div>


      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->