
    



  <!--****************************************************** 1ra sessao  *****************************************************************************-->
        
    <div class="page-section about-us-2-cont pt-42-cont">
      <div class="container">
      
        <div class="row">
          <div class="col-md-12">
            <div class="mb-40">
              <h2 class="section-title">OUR <span class="bold">TEAM</span></h2>
            </div>
          </div>
        </div>
        
      </div>
    </div>

           
    <div class="page-section pt-110-b-30-cont">
      <div class="container">

          <div class="row mb-30" >
            <div class="owl-clients-no-auto-solucoes owl-carousel owl-white-bg pb-30" >

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500 uppercase" href="shop-single.html">Cirurgia</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">A Cirurgia Oral permite corrigir alterações e patologias da cavidade oral.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500" href="shop-single.html">Endodontia</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">O tratamento de canal soluciona problemas da parte interna do dente.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500" href="shop-single.html">Buco</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">Trate problemas de mastigação, elimine as dores no maxilar e corrija imperfeições.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500" href="shop-single.html">Periodontia</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">Responsável pela prevenção e tratamento de doenças que afetam as gengivas e o osso dentário.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500" href="shop-single.html">Higiene Oral</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">Dentes e gengivas limpos e saudáveis evitam tártaro, gengivite, periodontite, cárie e outras doenças.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

              <div class="item text-center bg-box">      
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm- a-inv red-padrao fw-500" href="shop-single.html">Restauração</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <p style="margin: 0 17px;">Remoção da parte do dente que está danificada e preenchimento da cavidade limpa com um material de restauração.</p>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium cta-franqueado-gray gray-light shop-add-btn btn-obalo uppercase smooth-scroll" href="#">saiba <strong>mais</strong></a>
                  </div>                  
                </div>
              </div>

                            
             
            </div>
          </div>

      </div>
    </div>

    <div class="page-section pt-110-b-30-cont">
      <div class="container">

          <div class="row mb-30" >
            <div class="owl-clients-auto-2 owl-carousel" >
              <div class="item text-center">
                <img src="<?php base_url('img-teste.png','img') ?>" alt="client">

                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo cyan btn-5 btn-5aa" href="#">
                      <span aria-hidden="true" class="button-icon-anim icon icon-ecommerce-dollar"></span>  
                      <span class="button-text-anim ">BUTTON</span>
                    </a>
                  </div>                  
                </div>

              </div>
              <div class="item text-center">
                <img src="<?php base_url('img-teste.png','img') ?>" alt="client">
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('img-teste.png','img') ?>" alt="client">
                <div class="post-prev-title mb-5  mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('img-teste.png','img') ?>" alt="client">
                <div class="post-prev-title mb-5  mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>              
             
            </div>
          </div>

      </div>
    </div>

     <!--carrosel a pantalla completa-->

      
    <div class="white-bg- plr-30- pt-50 pb-0 pt-20-mobil" style="<?php echo isMobile() ? 'width: 95%' : 'width: 99%' ?>">
        
          <div class="relative">

          <div class="row mb-30" >
            <div class="owl-clients-auto-teste owl-carousel" >
                            
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 
              
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 
              
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 

              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 

              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div>         
               
            </div>
          </div>

      </div>
    </div>


    <!--carrosel en el centro de la pagina-->

    <div class="page-section pt-110-b-30-cont">
      <div class="container">

          <div class="row mb-30" >
            <div class="owl-clients-auto-teste owl-carousel" >
                            
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 
              
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 
              
              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 

              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div> 

              <div class="item port-item-custom mix">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-teste.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span style="color: #fff;">photography<span class="slash-divider"></span style="color: #fff;"></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </div>         
               
            </div>
          </div>

      </div>
    </div>


<!--****************************************************** 2da sessao  *****************************************************************************-->

    <!-- FEATURES 9 -->
        <div class="page-section p-80-cont">
        
          <div class="container">
            <div class="row">

              <div class="col-md-4">
                <div class="mt-60 mb-80">
                  <h2 class="section-title">DO YOU WANT<br><span class="bold">WEBSITE</span>?</h2>
                </div>

                <div class="row">
                        
                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec.
                    </p>   

                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum.
                    </p>                    
                          
                </div>
                        
                           
              </div>
            
              <div class="col-md-8 fes9-img-cont clearfix">
                <div class="fes9-img-center clearfix">
                  <div class="fes9-img-center">
                    <img src="<?php base_url('tel-1.png','img') ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s">
                    <img src="<?php base_url('tel-2.png','img') ?>" alt="img" class=" wow fadeInUp" data-wow-delay="450ms" data-wow-duration="1s">
                  </div>
                </div>
              </div>
            
            </div>

             <!-- FEATURES 8-->
            <div class="row mt-100">
           
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn">
                  <div class="fes8-title-cont pl-0" >
                    <!--<div class="fes8-box-icon">
                      <div class="icon icon-basic-smartphone"></div>
                    </div>-->
                    <h3>RETINA READY GRAPHICS</h3>
                  </div>
                  <div>
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="200ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-mixer2"></div>
                    </div>
                    <h3>PARALLAX SUPPORT</h3>
                  </div>
                  <div>
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="400ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-share"></div>
                    </div>
                    <h3>ENDLESS POSSIBILITIES</h3>
                  </div>
                  <div>
                     Seductio maesto nisi in sem fermentum blat. In nec elit solliudin, elementum.
                  </div>
                </div>
              </div>
              
            </div>


          </div>
        </div>


<!--****************************************************** 3ra sessao  *****************************************************************************-->        

        <div class="page-section about-us-2-cont- pt-42-cont">
          <div class="container">
          
            <div class="row">
              <div class="col-md-12">
                <div class="mb-40">
                  <h2 class="section-title">SOLUÇÕES <span class="bold">OFERECIDAS</span></h2>
                </div>
              </div>
            </div>
            
          </div>
        </div>


        <div class="white-bg- plr-30- pt-50 pb-30">
        
          <div class="relative">
            <!-- PORTFOLIO FILTER -->                    
            <div class="port-filter text-center text-left-767">
              <a href="#" class="filter active" data-filter="*">All Projects</a>
              <a href="#" class="filter" data-filter=".development">Development</a>
              <a href="#" class="filter" data-filter=".design">Design</a>
              <a href="#" class="filter" data-filter=".photography">Photography</a>
            </div>                   
                    
            <!-- ITEMS GRID -->
            <ul class="port-grid display-hover-on-mobile masonry clearfix" id="items-grid-2">  

              <!-- Item  big -->
              <li class="port-item mix design">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span><a href="#">photography</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
         
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-3.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                            
              <!-- Item  -->
              <li class="port-item mix design">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-7.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
 
              <!-- Item  big -->
              <li class="port-item mix design">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">CALENDAR</a></h3>
                      <span><a href="#">photography</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-6-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-1.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                             
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-4.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                    
              <!-- Item  big -->
              <li class="port-item mix development">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">NOW IS NOW</a></h3>
                      <span><a href="#">design</a><span class="slash-divider">/</span><a href="#">photography</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-5-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              <!-- Item   -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">LOVE</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-5.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>                                      
        
            </ul>
          
          </div>
        
        </div>


<!--****************************************************** 4ta sessao PARALAX *****************************************************************************-->


<!--****************************************************** 5ta sessao  *****************************************************************************-->  


        <div class="page-section about-us-2-cont- pt-42-cont">
          <div class="container">
          
            <div class="row">
              <div class="col-md-12">
                <div class="mb-40">
                  <h2 class="section-title">VANTAGENS <span class="bold">E BENEFÍCIOS</span></h2>
                </div>
              </div>
            </div>
            
          </div>
        </div>


        <!-- FEATURES 8 -->
        <div class="page-section fes4-cont">
          <div class="container">
            <div class="row">
           
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-smartphone"></div>
                    </div>
                    <h3>RETINA READY GRAPHICS</h3>
                  </div>
                  <div>
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="200ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-mixer2"></div>
                    </div>
                    <h3>PARALLAX SUPPORT</h3>
                  </div>
                  <div>
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="400ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-share"></div>
                    </div>
                    <h3>ENDLESS POSSIBILITIES</h3>
                  </div>
                  <div>
                     Seductio maesto nisi in sem fermentum blat. In nec elit solliudin, elementum.
                  </div>
                </div>
              </div>
              
            </div>
            <div class="row">
            
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="600ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-paperplane"></div>
                    </div>
                    <h3>CLEAR DESIGN</h3>
                  </div>
                  <div>
                    Donec vel luctus nisi in sem fermentum blat. In nec elit solliudin, elementum.
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="800ms" >
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-chronometer"></div>
                    </div>
                    <h3>POWERFUL PERFORMANCE</h3>
                  </div>
                  <div>
                    Lorem luctus antena at nisi in sem blandit. In nec elit soltudin, elementum odio. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="1000ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-software-horizontal-align-center"></div>
                    </div>
                    <h3>FLEXIBLE WIDGETS</h3>
                  </div>
                  <div>
                    Fermentum nisi in sem fertum blat. In elit soldin, elemtum, arenam pur quam volut.
                  </div>
                </div>
              </div>              
            
            </div>
            
            <div class="row">
            
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="600ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-laptop"></div>
                    </div>
                    <h3>RESPONSIVE LAYOUT</h3>
                  </div>
                  <div>
                    Donec vel luctus nisi in sem fermentum blat. In nec elit solliudin, elementum, dictum pur quam volutpat suscipit antena.
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="800ms" >
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-music-play-button"></div>
                    </div>
                    <h3>HTML5 VIDEO</h3>
                  </div>
                  <div>
                    Lorem luctus antena at nisi in sem blandit. In nec elit soltudin, elementum odio et, dictum quam a volutpat elementum. 
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="fes8-box wow fadeIn" data-wow-delay="1000ms">
                  <div class="fes8-title-cont" >
                    <div class="fes8-box-icon">
                      <div class="icon icon-basic-info"></div>
                    </div>
                    <h3>WELL DOCUMENTED</h3>
                  </div>
                  <div>
                    Fermentum nisi in sem fertum blat. In elit soldin, elemtum, arenam pur quam volutpat suscipit dictum pur quam.
                  </div>
                </div>
              </div>              
            
            </div>
          </div>
        </div>

<!--****************************************************** 6ta sessao  *****************************************************************************--> 

        <div class="page-section p-80-cont">
        
          <div class="container">
            <div class="row">

              <div class="col-md-4">
                <div class="mt-60 mb-80">
                  <h2 class="section-title">EXPERTICE<br><span class="bold">INTERNACIONAL</span>?</h2>
                </div>

                <div class="row">
                        
                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec.
                    </p>   

                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum.
                    </p>                    
                          
                </div>
                        
                           
              </div>
            
              <div class="col-md-8 fes9-img-cont clearfix">
                <div class="fes9-img-center clearfix">
                  <div class="fes9-img-center">
                    <img src="<?php base_url('tel-1.png','img') ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s">
                    <img src="<?php base_url('tel-2.png','img') ?>" alt="img" class=" wow fadeInUp" data-wow-delay="450ms" data-wow-duration="1s">
                  </div>
                </div>
              </div>
            
            </div>

          </div>
        </div>

<!--****************************************************** 7ma sessao  *****************************************************************************-->

        <div class="page-section p-80-cont">
        
          <div class="container">
            <div class="row">

              <div class="col-md-4">
                <div class="mt-60 mb-80">
                  <h2 class="section-title">EQUIPE<br><span class="bold">MÉDICA</span>?</h2>
                </div>

                <div class="row">
                        
                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec.
                    </p>   

                    <p class="pl-5">
                     Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum. Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum.
                    </p>                    
                          
                </div>
                        
                           
              </div>
            
              <div class="col-md-8 fes9-img-cont clearfix">
                <div class="fes9-img-center clearfix">
                  <div class="fes9-img-center">
                    <img src="<?php base_url('tel-1.png','img') ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s">
                    <img src="<?php base_url('tel-2.png','img') ?>" alt="img" class=" wow fadeInUp" data-wow-delay="450ms" data-wow-duration="1s">
                  </div>
                </div>
              </div>
            
            </div>

          </div>
        </div>
        
<!--****************************************************** 8va sessao  *****************************************************************************-->


        <!-- PORTFOLIO SECTION 3 CAR-->
        <div class="page-section">
          <div class="relative">
          
            
            <ul class="port-grid clearfix" id="items-grid">

              
              <li class="port-item port-item-width-2 mix">
                <div class="port-text-cont relative">
                  <img class="port-main-img" src="<?php base_url('car-bg-wide.png','img') ?>" alt="img" >
                    <div class="block-center-xy">
                      <div class="title-fs-45-wide">
                        OPTIMIZED&nbsp;FOR<br>
                        YOUR&nbsp;DESIRES
                      </div>
                    </div>

                </div>
              </li>   
              
              
              <li class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('car-1.jpg','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">MINIMALISM BOOKS</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('car-1.jpg','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              
              <li class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('car-2.jpg','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">CALENDAR</a></h3>
                      <span><a href="#">photography</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('car-2.jpg','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              
              
              <li class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('car-3.jpg','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('car-3.jpg','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              
              
              <li class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('car-4.jpg','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">NOW IS NOW</a></h3>
                      <span><a href="#">design</a><span class="slash-divider">/</span><a href="#">photography</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('car-4.jpg','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              
              
              <li class="port-item port-item-width-2 mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('car-5.jpg','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('car-5.jpg','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              

            </ul>
          
          </div>
        </div> 


<!--****************************************************** 9na sessao  *****************************************************************************-->

        <!-- FEATURES 2 -->
        <div class="page-section">
          <div class="container-fluid">
            <div class="row">
            
              <div class="col-md-6 wow fadeInLeft equal-height fes2-img-home-mobil">
                <div class="fes2-main-text-cont">
                  <div class="title-fs-45 text-left">
                    MORE THAN<br>
                    <span class="bold">RESPONSIVE</span>
                  </div>
                  <div class="line-3-70-"></div>
                  <div class="fes2-text-cont text-left">
                    <p class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus  antium que. Asperiores, ea velit enim labore doloribus.</p> 

                    <a class="button medium cta-franqueado tp-button btn-obalo mt-15" href="https://1.envato.market/GVZ26">MARCAR AVALIAÇÃO</a>

                  </div>
                </div>
              </div>
              
              <div class="col-md-6 desktopOnly">
                <div class="row">
                  <div class="fes2-img-home equal-height"></div>
                </div>
              </div>
              
            </div>
          </div>
        </div>

    <!--================1ra sessao =================-->
    <div id="about" class="page-section">
        <div class="container fes1-cont">
            <div class="row">

                <div class="col-md-4 fes1-img-cont wow fadeInUp mb-20">
                    <img src="<?php base_url('phone-with-arrow.png','img') ?>" alt="img" >
                </div>

                <div class="col-md-8">

                    <div class="row">
                      <div class="col-md-12">
                            <div class="fes1-main-title-cont wow fadeInDown">
                                <div class="title-fs-60">
                                    WE ARE<br>
                                    <span class="bold">CREATIVE</span>
                                </div>
                                <div class="line-3-100"></div>
                            </div>
                      </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <div class="fes1-box wow fadeIn" data-wow-delay="1ms">
                                <div class="fes1-box-icon">
                                    <!--<div class="icon icon-basic-mixer2"></div> -->
                                    <img src="<?php base_url('icons_mercado-05.png','img') ?>" style="width: 40%;">
                                </div>
                                <h3>FULLY RESPONSIVE</h3>
                                <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="fes1-box wow fadeIn" data-wow-delay="200ms">
                                <div class="fes1-box-icon">
                                    <div class="icon icon-basic-lightbulb"></div>
                                </div>
                                <h3>RETINA READY</h3>
                                <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                      <div class="col-md-6 col-sm-6">
                        <div class="fes1-box wow fadeIn" data-wow-delay="400ms">
                            <div class="fes1-box-icon">
                                <div class="icon icon-basic-helm"></div>
                            </div>
                            <h3>UNIQUE DESIGN</h3>
                            <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                        </div>
                      </div>

                       <div class="col-md-6 col-sm-6">
                            <div class="fes1-box wow fadeIn"  data-wow-delay="600ms">
                                <div class="fes1-box-icon">
                                    <div class="icon icon-basic-settings"></div>
                                </div>
                                <h3>EASY TO CUSTOMIZE</h3>
                                <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                            </div>
                       </div>

                    </div>                    

                </div>

            </div>
        </div>
    </div>



    <!-- COUNTERS 1 -->
    <div id="counter-1" class="page-section p-80-cont">
      <div class="container">
    
        <div  class="row text-center">
                        
          <!-- Item -->
          <div class="col-xs-6 col-sm-3">
            <div class="count-number">
              75
            </div>
            <div class="count-descr">
              <span class="count-title">AWARDS WINNING</span>
            </div>
          </div>
          
          <!-- Item -->
          <div class="col-xs-6 col-sm-3">
            <div class="count-number">
             450
            </div>
            <div class="count-descr">
              <span class="count-title">HAPPY CLIENTS</span>
            </div>
          </div>
          
          <!-- Item -->
          <div class="col-xs-6 col-sm-3">
            <div class="count-number">
              151
            </div>
            <div class="count-descr">
              <span class="count-title">PROJECTS DONE</span>
            </div>
          </div>
          
          <!-- Item -->
          <div class="col-xs-6 col-sm-3">
            <div class="count-number">
             768
            </div>
            <div class="count-descr">
              <span class="count-title">HOURS OF CODE</span>
            </div>
          </div>  
          
        </div>
      </div>
    </div>




    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">  

    <!-- ADS 1 -->
    <div class="page-section">
        <div class="container">
            <div class="row">
                
                <div class="col-md-6 left-50">
                    <div class="fes2-main-text-cont">
                        <div class="title-fs-45">
                            OPTIMIZED FOR<br>
                            <span class="bold">MOBILE</span>
                        </div>
                        <div class="line-3-100"></div>
                        <div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus  antium que. Asperiores, ea velit enim labore doloribus.</div>
                        <div class="mt-30">
                          <a class="button medium thin hover-dark" href="https://1.envato.market/GVZ26">BUY NOW</a>
                        </div>
                    </div>
                </div>
  
                <div class="col-md-6 right-50 wow fadeInLeft">
                            
                    <div class="ads-img-cont" >
                        <img src="<?php base_url('img_banner_7.png','img') ?>" alt="img">
                    </div>
    
                </div>
  
            </div>
        </div>
    </div> 

    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">          

    <!-- ADS 2 -->
    <div class="page-section">
        <div class="container">
            <div class="row">
                
                <div class="col-md-6  ">
                    <div class="fes2-main-text-cont">
                        <div class="title-fs-45">
                            POWERFUL<br>
                            <span class="bold">OPTIONS</span>
                        </div>
                        <div class="line-3-100"></div>
                        <div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus  antium que. Asperiores, ea velit enim labore doloribus.</div>
                        <div class="mt-30">
                          <a class="button medium thin hover-dark" href="https://1.envato.market/GVZ26">BUY NOW</a>
                        </div>
                    </div>
                </div>
  
                <div class="col-md-6 wow fadeInRight">
                                
                    <div class="ads-img-cont" >
                        <img src="<?php base_url('img_banner_7.png','img') ?>" alt="img">
                    </div>
                
                </div>
  
            </div>
        </div>
    </div>





    <!--<div class="grey-light-bg plr-30 plr-0-767 clearfix">-->    
        <!-- COTENT CONTAINER -->
        <div class="white-bg- plr-30 pt-50 pb-30">
        
          <div class="relative">
            <!-- PORTFOLIO FILTER -->                    
            <div class="port-filter text-center text-left-767">
              <a href="#" class="filter active" data-filter="*">All Projects</a>
              <a href="#" class="filter" data-filter=".development">Development</a>
              <a href="#" class="filter" data-filter=".design">Design</a>
              <a href="#" class="filter" data-filter=".photography">Photography</a>
            </div>                   
                    
            <!-- ITEMS GRID -->
            <ul class="port-grid port-grid-5 masonry clearfix" id="items-grid">

              <!-- Item  big -->
              <li class="port-item mix design">
                <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20">CALENDAR</a></h3>
                      <span><a href="#">photography</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades-a.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com.br/maps/place/Swiss+Dental+Services+Porto/@41.1581853,-8.6458781,17z/data=!3m1!4b1!4m5!3m4!1s0xd24659ec87885e3:0xbac1db3f5aed48eb!8m2!3d41.1581813!4d-8.6436894?hl=pt-PT&authuser=0" target="_blank" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
         
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-3.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                            
              <!-- Item  -->
              <li class="port-item mix design">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-7.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
 
              <!-- Item  big -->
              <li class="port-item mix design">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">CALENDAR</a></h3>
                      <span><a href="#">photography</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-6-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
              
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-1.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                             
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-4.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                    
              <!-- Item  big -->
              <li class="port-item mix development">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">NOW IS NOW</a></h3>
                      <span><a href="#">design</a><span class="slash-divider">/</span><a href="#">photography</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-5-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              <!-- Item   -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">LOVE</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-5.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              <!-- Item -->
              <li class="port-item mix development">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">MINIMALISM BOOKS</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-9.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
                             
              <!-- Item  -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-10.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
        
              <!-- Item   -->
              <li class="port-item mix photography">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">LOVE</a></h3>
                      <span><a href="#">branding</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-11.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              <!-- Item -->
              <li class="port-item mix development">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('unidades.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">MINIMALISM BOOKS</a></h3>
                      <span><a href="#">ui elements</a><span class="slash-divider">/</span><a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="images/portfolio/projects-12.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>
               
            </ul>
          
          </div>
        
        </div>
        
      <!--</div>-->

        <!--sessao-->
        <div class="page-section">
            <div class="container-fluid">
                <div class="row row-sm-fix">
                    
                    <div class="col-md-6 fes12-img equal-height" style="background-image: url('<?php base_url('5.jpg','img') ?>'); height: 552px;">
                        <div class="fes2-main-text-cont text-white">
                          <div class="fes2-title-45 font-poppins text-white">
                            <strong>Optimized for<br>mobile</strong>
                          </div>
                          <div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus antium que. Asperiores, ea velit enim labore doloribus.</div>
                          <div class="fes12-btn-cont mt-30">
                            <a class="button medium white rounded thin btn-4 btn-4cc" href="#"><span class="button-text-anim">READ MORE</span><span aria-hidden="true" class="button-icon-anim arrow_right"></span></a>
                          </div>
                        </div>
                    </div>
                      
                    <div class="col-md-6 fes12-img equal-height" style="background-image: url('<?php base_url('4.jpg','img') ?>'); height: 552px;">
                        <div class="fes2-main-text-cont text-black">
                          <div class="fes2-title-45 font-poppins">
                            <strong>Powerful<br>Performance</strong>
                          </div>
                          
                          <div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus antium que. Asperiores, ea velit enim labore doloribus.</div>
                          <div class="fes12-btn-cont mt-30">
                            <a class="button medium rounded thin gray btn-4 btn-4cc" href="#"><span class="button-text-anim">READ MORE</span><span aria-hidden="true" class="button-icon-anim arrow_right"></span></a>
                          </div>
                        </div>
                    </div>
                      
                </div>

            </div>
        </div>

        <!--sessao para modificar com boton-->
        <div class="page-section pt-160-b-120-cont">
          <div class="container">
            <div class="row">
              
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" style="visibility: visible; animation-name: fadeIn; height: 274px; display: flex; flex-wrap: wrap; align-content: space-between; justify-content: center;">
                      <div class="fes17-title-cont">
                        <div class="fes17-box-icon">
                            <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 50%;">
                        </div>
                        <h3><strong>Marketing</strong></h3>
                      </div>
                      <div class="text-center">
                        Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat  
                      </div>

                      <div class="row mt-20" style="display: flex; justify-content: center;">                    
                        <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)" style="width: 158px;">Saiba mais</a>
                      </div>
                  </div>

                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeIn; height: 274px; display: flex; flex-wrap: wrap; align-content: space-between; justify-content: center;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 40%;">
                      </div>
                      <h3><strong>Development</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)" style="width: 158px;">Saiba mais</a>
                    </div>

                  </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn; height: 274px; display: flex; flex-wrap: wrap; align-content: space-between; justify-content: center;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 50%;">
                      </div>
                      <h3><strong>Production</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat. Proin malesuada quam a volutpat. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)" style="width: 158px;">Saiba mais</a>
                    </div>
                  </div>

                </div>
              
            </div>
            <div class="row">
            
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeIn;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 15%;">
                      </div>
                      <h3><strong>Branding</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)">Saiba mais</a>
                    </div>
                  </div>

                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="800ms" style="visibility: visible; animation-delay: 800ms; animation-name: fadeIn;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 15%;">
                      </div>
                      <h3><strong>Web Design</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)">Saiba mais</a>
                    </div>
                  </div>

                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 1000ms; animation-name: fadeIn;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 15%;">
                      </div>
                      <h3><strong>Photography</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)">Saiba mais</a>
                    </div>
                  </div>

                </div>             
                        
            </div>

            <!--testando columnas en el medio-->

            <div class="row">
            
                <div class="col-xs-12 col-sm-4 col-md-4">
                  

                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="mb-70 wow fadeIn" data-wow-delay="800ms" style="visibility: visible; animation-delay: 800ms; animation-name: fadeIn;">
                    <div class="fes17-title-cont">
                      <div class="fes17-box-icon">
                        <img src="<?php base_url('icon-home-quemsomos_1.png','img') ?>" style="width: 15%;">
                      </div>
                      <h3><strong>Web Design</strong></h3>
                    </div>
                    <div class="text-center">
                      Maecenas luctus nisi in sem fermentum blandit. In nec elit sollicitudin, elementum odio et, dictum purus. Proin malesuada quam a volutpat  
                    </div>

                    <div class="row mt-20" style="display: flex; justify-content: center;">                    
                      <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil-solucoes" href="javascript: void(0)">Saiba mais</a>
                    </div>
                  </div>

                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                 

                </div>             
                        
            </div>




          </div>
        </div>



                <!--sessao-->

                <div class="page-section bg-gray-light clearfix">
          <div class="fes7-img-cont col-md-5">
            <div class="fes7-img" style="background-image: url(<?php base_url('6.jpg','img') ?>)"></div>
          </div>
          
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-6 fes7-text-cont p-80-cont">
                <h1><span class="font-light">Nulla varius faucibus vestibulum. Sed imperdiet, tellus at iaculis</span></h1>
                <p class="mb-60">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                </p>
              
                <div class="row">
                    
                  <div class="col-md-6 col-sm-6">
                    <div class="fes7-box wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                      <div class="fes7-box-icon">
                        <div class="icon icon-ecommerce-graph-increase"></div>
                      </div>
                      <h3>SEO Friendly</h3>
                      <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                    </div>
                  </div>
                      
                  <div class="col-md-6 col-sm-6">
                    <div class="fes7-box wow fadeIn" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeIn;">
                      <div class="fes7-box-icon">
                        <div class="icon icon-software-font-smallcaps"></div>
                      </div>
                      <h3>Google Fonts</h3>
                      <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                    </div>
                  </div>
                      
                </div>
                    
                <div class="row">
                    
                  <div class="col-md-6 col-sm-6">
                    <div class="fes7-box wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn;">
                      <div class="fes7-box-icon">
                        <div class="icon icon-basic-mixer2"></div>
                      </div>
                      <h3>Tons of Shortcodes</h3>
                      <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                    </div>
                  </div>
                      
                  <div class="col-md-6 col-sm-6">
                    <div class="fes7-box wow fadeIn" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeIn;">
                      <div class="fes7-box-icon">
                        <div class="icon icon-basic-bolt"></div>
                      </div>
                      <h3>1500+ Icons</h3>
                      <p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
                    </div>
                  </div>
                  
                </div> 
            
              </div>
            </div><!--end of row-->
          </div>
        </div>


        <!--sessao-->
        <div class="page-section pt-0 p-140-cont" style="margin-top: 5%;">
                    <div class="container">
            <div class="row mb-100">
            
              <div class="member col-md-4 col-sm-4 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('8.jpg','img') ?>" alt="img">
                </div>
                <h3>Jessica Atkinson</h3>
                <span>VP Engineering</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
              <div class="member col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('9.jpg','img') ?>" alt="img">
                </div>
                <h3>Ronald Jackson</h3>
                <span>Founder And Ceo</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
              <div class="member col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('10.jpg','img') ?>" alt="img">
                </div>
                <h3>Henry Shelton</h3>
                <span>Creative Director</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
            </div>
            
            <div class="row">
            
              <div class="member col-md-3 col-sm-3 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('11.jpg','img') ?>" alt="img">
                </div>
                <h3>Emma Griffith</h3>
                <span>QA Engineering</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
              <div class="member col-md-3 col-sm-3 wow fadeInUp" data-wow-delay="200ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('12.jpg','img') ?>" alt="img">
                </div>
                <h3>Brian Green</h3>
                <span>Android Developer</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
              <div class="member col-md-3 col-sm-3 wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 800ms; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('13.jpg','img') ?>" alt="img">
                </div>
                <h3>Mary Black</h3>
                <span>HR Manager</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>

              <div class="member col-md-3 col-sm-3 wow fadeInUp" data-wow-delay="400ms" style="visibility: visible; animation-delay: 1000ms; animation-name: fadeInUp;">
                <div class="member-image">
                  <img class="img-circle" src="<?php base_url('14.jpg','img') ?>" alt="img">
                </div>
                <h3>Paul Bishop</h3>
                <span>iOS Developer</span>
                <ul class="team-social">
                  <li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
                  <li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
                </ul>
              </div>
              
            </div>
                    </div>
        </div>



        <!--sessao portfolios-->
        <!-- PORTFOLIO SECTION 1 GALLERY -->
        <div class="page-section mb-50">
          <div class="relative">

             <!-- PORTFOLIO FILTER -->                    
            <div class="port-filter text-center text-left-767">
              <a href="#" class="filter active" data-filter="*">All Projects</a>
              <a href="#" class="filter" data-filter=".development">Development</a>
              <a href="#" class="filter" data-filter=".design">Design</a>
              <a href="#" class="filter" data-filter=".photography">Photography</a>
            </div> 

            <!-- ITEMS GRID -->
            <ul class="port-grid display-hover-on-mobile clearfix masonry" id="items-grid2">
                
              <!-- Item 1 -->
              <li role="button" class="port-item mix design">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">MINIMALISM BOOKS</a></h3>
                      <span><a href="#">ui elements</a> / <a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-1.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/blog/post-wide-3.jpg"></a>
                        <a href="images/blog/post-wide-4.jpg"></a>
                        <a href="images/blog/post-wide-5.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>

                </div>
              </li>

              <!-- Item 2 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
                 
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">CALENDAR</a></h3>
                      <span><a href="#">photography</a> / <a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-3-big.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/blog/post-prev-1.jpg"></a>
                        <a href="images/blog/post-prev-2.jpg"></a>
                        <a href="images/blog/post-prev-3.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
              
                </div>
              </li>
              
              <!-- Item 3 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
              
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">branding</a> / <a href="#">marketing</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-5-very-big.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/portfolio/car-2.jpg"></a>
                        <a href="images/portfolio/car-3.jpg"></a>
                        <a href="images/portfolio/car-4.jpg"></a>
                      </span> 
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
              
                </div>
              </li>
              
              <!-- Item 4 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
                 
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">NOW IS NOW</a></h3>
                      <span><a href="#">design</a> / <a href="#">photography</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-6-big.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/portfolio/projects-6.jpg"></a>
                        <a href="images/portfolio/projects-6-box.jpg"></a>
                        <a href="images/portfolio/projects-8.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
              
                </div>
              </li>
              
              <!-- Item 5 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
            
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
                      <span><a href="#">ui elements</a> / <a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-2-big.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/portfolio/projects-9.jpg"></a>
                        <a href="images/portfolio/projects-10.jpg"></a>
                        <a href="images/portfolio/projects-11.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
            
                </div>
              </li>
              
              <!-- Item 6 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
               
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">LOVE</a></h3>
                      <span><a href="#">branding</a> / <a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-4.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/blog/post-prev-4.jpg"></a>
                        <a href="images/blog/post-prev-5.jpg"></a>
                        <a href="images/blog/post-prev-6.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
             
                </div>
              </li>
              
              <!-- Item 7 -->
              <li role="button" class="port-item mix">
                <a href="portfolio-single1.html">
                  <div class="port-img-overlay"><img class="port-main-img" src="<?php base_url('15.png','img') ?>" alt="img" ></div>
                </a>
                <div class="port-overlay-cont">
               
                    <div class="port-title-cont">
                      <h3><a href="portfolio-single1.html">YELLOW BOOK</a></h3>
                      <span><a href="#">design</a> / <a href="#">media</a></span>
                    </div>
                    <div class="port-btn-cont">
                      <span class="popup-multi-gallery">
                        <a href="images/portfolio/projects-7.jpg" class="lightbox-item mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
                        <a href="images/portfolio/projects-12.jpg"></a>
                        <a href="images/portfolio/projects-13.jpg"></a>
                        <a href="images/portfolio/projects-14.jpg"></a>
                      </span>
                      <a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
                    </div>
          
                </div>
              </li>
               
            </ul>
          </div>
        </div>
        



        <!--TESTE promoções con imagen texto e botão-->
        <!-- SHOP ITEMS -->
        <div class="page-section pt-110-b-30-cont">
          <div class="container">
                
                       
            <div class="row">
              
              
              <!-- SHOP Item 1 -->
              <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" >
                  
                <div class="post-prev-img">
                  <a href="shop-single.html"><img src="<?php base_url('venda-1.jpg','img') ?>" alt="img"></a>
                </div>
                <div class="sale-label-cont">
                  <span class="sale-label label-danger bg-red">SALE</span>
                </div>  
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">WOMEN'S SHOES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <del>$130.00</del>&nbsp;<strong>$98.55</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="shop-add-btn-cont">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              
              </div>
              
              <!-- SHOP Item 2 -->
              <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" data-wow-delay="200ms" >
                  
                <div class="post-prev-img">
                  <a href="shop-single.html"><img src="<?php base_url('venda-2.jpg','img') ?>" alt="img"></a>
                </div>
                  
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="shop-add-btn-cont">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              
              </div>
              
              <!-- SHOP Item 3 -->
              <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" data-wow-delay="400ms" >
                  
                <div class="post-prev-img">
                  <a href="shop-single.html"><img src="<?php base_url('venda-1.jpg','img') ?>" alt="img"></a>
                </div>
                  
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">HEM SHELL TOP</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$100.75</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="shop-add-btn-cont">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              
              </div>
              
              <!-- SHOP Item 4 -->
              <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" data-wow-delay="600ms">
                  
                <div class="post-prev-img">
                  <a href="shop-single.html"><img src="<?php base_url('venda-2.jpg','img') ?>" alt="img"></a>
                </div>
                  
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">MEN'S SHOES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$80.25</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="shop-add-btn-cont">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              
              </div>

              <!-- SHOP Item 5 -->
              <div class="col-sm-6 col-md-3 col-lg-3 wow fadeIn pb-70" data-wow-delay="600ms">
                  
                <div class="post-prev-img">
                  <a href="shop-single.html"><img src="<?php base_url('venda-1.jpg','img') ?>" alt="img"></a>
                </div>
                  
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">MEN'S SHOES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$80.25</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="shop-add-btn-cont">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              
              </div>

           
            </div>
          </div>
        </div>


        <!-- OUTRO TESTE -->
    <div class="page-section pt-110-b-30-cont">
      <div class="container">
        <h4 class="mb-30">Multiple items with auto play</h4>  
          <div class="row mb-30" >
            <div class="owl-clients-auto-2 owl-carousel" >
              <div class="item text-center">
                <img src="<?php base_url('venda-5.jpg','img') ?>" alt="client">

                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo cyan btn-5 btn-5aa" href="#">
                      <span aria-hidden="true" class="button-icon-anim icon icon-ecommerce-dollar"></span>  
                      <span class="button-text-anim ">BUTTON</span>
                    </a>
                  </div>                  
                </div>

              </div>
              <div class="item text-center">
                <img src="<?php base_url('venda-6.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5 mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('venda-5.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5  mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('venda-6.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5  mt-10">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix" style="width: 60%;  margin: 0 auto;">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn btn-obalo" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
             <!-- <div class="item text-center">
                <img src="<?php base_url('venda-1.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('venda-2.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div>
              <div class="item text-center">
                <img src="<?php base_url('venda-3.jpg','img') ?>" alt="client">
                <div class="post-prev-title mb-5">
                  <h3><a class="font-norm a-inv" href="shop-single.html">SUNGLASSES</a></h3>
                </div>
                  
                <div class="shop-price-cont">
                  <strong>$90.15</strong>
                </div>
                  
                <div class="post-prev-more-cont clearfix">
                  <div class="">
                    <a class="button medium gray-light shop-add-btn" href="#">ADD TO CART</a>
                  </div>
                  
                </div>
              </div> -->
            </div>
          </div>

      </div>
    </div>

    <!--================1ra sesao  br_btn white shop-add-btn-cont=================-->