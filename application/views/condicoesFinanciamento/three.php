
    <!--****************************************************** 3ra sessao  ******************************************************************-->        

    <section class="sec-padding section-light-">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 nopadding">
            <div class="sec-title-container text-center wow animated fadeInUp">
              <div class="ce4-title-line-1" style="background-color: #f50f40;"></div>
              <h4 class="uppercase font-weight-7 less-mar-1 font-size-title"><?=condicoes_three_text_1?></h4>
              <div class="clearfix"></div>
              <p class="by-sub-title"></p>
            </div>
          </div>
          <div class="clearfix"></div>
          <!--end title-->
          
          <div class="col-md-4 col-sm-6 col-xs-12 wow animated fadeInUp" data-wow-delay="0.0s">
            <div class="ce4-price-table-2 margin-bottom" style="background-color: #f5f5f5">
              <div class="inner-box text-center">
                <h4 class="uppercase less-mar-1 font-weight-6 title"><?=condicoes_three_text_2?> <br> <span class="sessao-3"><?/*=condicoes_three_text_2_1*/?></span> </h4>
                <br/>
                <div class="price-circle" style="padding: 18px 0 0;">
                  <div class="price"><i>Desde</i> <br> <?=condicoes_three_text_3?><sup><?=condicoes_three_text_3_1?></sup>€</div>
                  <i>mês</i></div>
                <br/>
                <ul class="plan_features">
                  <li><?=condicoes_three_text_4?></li>
                  <li class="highlight"><?=condicoes_three_text_5?></li>
                  <li><?=condicoes_three_text_6?></li>
                </ul>
                <div class="clearfix"></div>
                <!--<a class="btn-2 btn-red uppercase" href="#">Order Now</a>--> </div>
            </div>
          </div>
          <!--end item-->
          
          <div class="col-md-4 col-sm-6 col-xs-12 wow animated fadeInUp" data-wow-delay="0.2s">
            <div class="ce4-price-table-2 active margin-bottom" style="background-color: #8b001d">
              <div class="inner-box text-center">
                <h4 class="uppercase less-mar-1 font-weight-6 title text-white"><?=condicoes_three_text_7?> <br> <span class="sessao-3"><?=condicoes_three_text_8?></span></h4>
                <br/>
                <div class="price-circle" style="padding: 18px 0 0;">
                  <div class="price" style="color: #f50f40;"><i>Desde</i> <br> <?=condicoes_three_text_9?><sup><?=condicoes_three_text_9_1?></sup>€</div>
                  <i style="color: #f50f40;">mês</i></div>
                <br/>
                <ul class="plan_features">
                  <li class="text-white"><?=condicoes_three_text_10?></li>
                  <li class="highlight text-white"><?=condicoes_three_text_11?></li>
                  <li class="text-white" ><?=condicoes_three_text_12?></li>
                </ul>
                <div class="clearfix"></div>
                <!--<a class="btn-2 btn-red uppercase" href="#">Order Now</a>--> </div>
            </div>
          </div>
          <!--end item-->
          
          <div class="col-md-4 col-sm-6 col-xs-12 wow animated fadeInUp" data-wow-delay="0.4s">
            <div class="ce4-price-table-2 margin-bottom" style="background-color: #f5f5f5">
              <div class="inner-box text-center">
                <h4 class="uppercase less-mar-1 font-weight-6 title"><?=condicoes_three_text_13?></h4>
                <br/>
                <div class="price-circle" style="padding: 18px 0 0;">
                  <div class="price"><i>Desde</i> <br> <?=condicoes_three_text_14?><sup><?=condicoes_three_text_15?></sup>€</div>
                  <i>mês</i></div>
                <br/>
                <ul class="plan_features">
                  <li><?=condicoes_three_text_16?></li>
                  <li class="highlight"><?=condicoes_three_text_17?></li>
                  <li><?=condicoes_three_text_18?></li>
                </ul>
                <div class="clearfix"></div>
                <!--<a class="btn-2 btn-red uppercase" href="#">Order Now</a>--> </div>
            </div>
          </div>
          <!--end item--> 
          
        </div>

         <div class="row wow animated fadeInUp" style="padding-top: 50px; display: flex; justify-content: center;" >
          <a href="#avaliacao" class="smooth-scroll btn-2 btn-red btn-2-border">Marcar avaliação</a>
        </div>

        <?php
            echo funGetSlide('condicoes_three','','','

            <div class="row content-custon wow animated fadeInUp">

              <p class="text-center">{{title}}</p>

              <p class="text-center">{{subtitle}}</p>

              <p class="text-center font-size-text">{{text}}</p>
              
            </div>
            ');
        ?>


      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section <sup>$</sup>-->