
   <!--****************************************************** 6ta sessao  EXPERIÊNCIA INTERNACIONAL***************************************************************--> 

        <div class="page-section p-80-cont p-80-cont-mobil">
        
          <div class="container">
            <div class="row">

              <div class="col-md-4 vertical">
                <div class="mt-60 mb-80">
                  <h2 class="section-title red-padrao uppercase"><?= home_six_text_1 ?><br><span class="bold"><?= home_six_text_2 ?></span></h2>
                </div>

                <div class="row">

                  <?php
                      echo funGetAdvancedBanners('home_six', '

                    <p class="pl-5 pr-5-mobil">
                     {{subtitle}}
                    </p>

                    <p class="pl-5 pr-5-mobil">
                     {{text}}
                    </p>   

                    <p class="pl-5 pr-5-mobil">
                     {{subtext}}
                    </p>                    
                                                               
                        ');
                  ?>
                        
                          
                </div>
                        
                           
              </div>
            
              <div class="col-md-8 fes9-img-cont clearfix desktopOnly" style="transform: translate(-106px, -156px);">
                <div class="fes9-img-center clearfix">
                  <div class="fes9-img-center">
                    <img src="<?php base_url('img-home-expertiseinternacional-parte-1.png','img') ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s" style="margin-top: 20%;  width: 225%; max-width: 225%;">
                    <img src="<?php base_url('img-home-expertiseinternacional-parte-2.png','img') ?>" alt="img" class=" wow fadeInUp" data-wow-delay="450ms" data-wow-duration="1s" style="margin-top: 20%;  width: 225%; max-width: 225%;">
                  </div>
                </div>
              </div>
            
            </div>

            <div class="row mt-30" style="display: flex; justify-content: center;">                    
                <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil" href="nossa-equipe">Saiba mais</a>
            </div>   

          </div>
        </div>