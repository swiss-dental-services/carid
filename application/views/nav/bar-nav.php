<!-- href="<?php /*base_url('suporte-fale-conosco')*/ ?>" class="<?php /*echo ($active_submenu == 'suport')? 'current' : ''*/ ?>" -->


<div class="col-md-12 nopadding">
      <div class="header-section style1 noborder no-bg fn-mar pin-style">
        <div class="container">
          <div class="mod-menu">
            <div class="row">
              <div class="col-sm-2"> <a href="<?php base_url('') ?>" title="" class="logo style-2 mar-4"> <img src="<?php base_url('Logo-Carid-03.svg','img/custon') ?>" alt="" class="img-logo" style="/*width: 138%; max-width: 150%;*/"> </a> </div>
              <div class="col-sm-10">
                <div class="main-nav">
                  <ul class="nav navbar-nav top-nav">
                    <!--<li class="search-parent"> <a href="javascript:void(0)" title="" class="m-link"><i aria-hidden="true" class="fa fa-search"></i></a>
                      <div class="search-box ">
                        <div class="content">
                          <div class="form-control">
                            <input type="text" placeholder="Type to search" />
                            <a href="#" class="search-btn mar-2"><i aria-hidden="true" class="fa fa-search"></i></a> </div>
                          <a href="#" class="close-btn mar-1">x</a> </div>
                      </div>
                    </li> -->
                    <!--<li class="cart-parent"> <a href="javascript:void(0)" title="" class="m-link"> <i aria-hidden="true" class="fa fa-shopping-cart"></i> <span class="number mar2"> 4 </span> </a>
                      <div class="cart-box">
                        <div class="content">
                          <div class="row">
                            <div class="col-xs-8"> 2 item(s) </div>
                            <div class="col-xs-4 text-right"> <span>$99</span> </div>
                          </div>
                          <ul>
                            <li> <img src="http://placehold.it/80x80" alt=""> Jean & Teashirt <span>$30</span> <a href="#" title="" class="close-btn">x</a> </li>
                            <li> <img src="http://placehold.it/80x80" alt=""> Jean & Teashirt <span>$30</span> <a href="#" title="" class="close-btn">x</a> </li>
                          </ul>
                          <div class="row">
                            <div class="col-xs-6"> <a href="#" title="View Cart" class="btn btn-block btn-warning">View Cart</a> </div>
                            <div class="col-xs-6"> <a href="#" title="Check out" class="btn btn-block btn-primary">Check out</a> </div>
                          </div>
                        </div>
                      </div>
                    </li>-->
                    <li class="visible-xs menu-icon"> <a href="javascript:void(0)" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false"> <i aria-hidden="true" class="fa fa-bars"></i> </a> </li>
                  </ul>
                  <div id="menu" class="collapse">
                    <ul class="nav navbar-nav">

                      <?php 

                      if(isMobile()){?>

                        <li class="mega-menu six-col <?php echo ($active == 'so')? 'active' : '' ?>"> 
                          <a href="<?php base_url('sobre') ?>" class="text-center">Sobre</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'cf')? 'active' : '' ?>"> 
                          <a href="<?php base_url('condicoes-financiamento') ?>" class="m-link text-center">Condições Financiamento </a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'ro')? 'active' : '' ?>"> 
                          <a href="<?php base_url('reabilitacao-oral') ?>" class="m-link text-center">Reabilitação Oral</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'cc')? 'active' : '' ?>"> 
                          <a href="<?php base_url('casos-clinicos') ?>" class="m-link text-center">Casos Clínicos</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'camp')? 'active' : '' ?>"> 
                          <a href="<?php base_url('campanhas') ?>" class="m-link text-center">Campanhas</a> <span class="arrow-"></span>
                          
                        </li>
                        <li class="<?php echo ($active == 'blog')? 'active' : '' ?>"> 
                          <a href="<?php base_url('blog') ?>" class="m-link text-center">Blog</a> <span class="arrow-"></span>
                          
                        </li>
                        <li class="<?php echo ($active == 'contato')? 'active' : '' ?>"> 
                          <a href="<?php base_url('contacto') ?>" class="m-link text-center">Contacto</a> <span class="arrow-"></span>
                          
                        </li>
                              
                      <?php    }else{ ?>

                        <li class="mega-menu six-col <?php echo ($active == 'so')? 'active' : '' ?>"> 
                          <a href="<?php base_url('sobre') ?>" class="text-center">Sobre</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'cf')? 'active' : '' ?>"> 
                          <a href="<?php base_url('condicoes-financiamento') ?>" class="m-link text-center">Condições<br>Financiamento </a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'ro')? 'active' : '' ?>"> 
                          <a href="<?php base_url('reabilitacao-oral') ?>" class="m-link text-center">Reabilitação<br>Oral</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'cc')? 'active' : '' ?>"> 
                          <a href="<?php base_url('casos-clinicos') ?>" class="m-link text-center">Casos<br>Clínicos</a> <span class="arrow-"></span>                        
                        </li>
                        <li class="<?php echo ($active == 'camp')? 'active' : '' ?>"> 
                          <a href="<?php base_url('campanhas') ?>" class="m-link text-center">Campanhas</a> <span class="arrow-"></span>
                          
                        </li>
                        <li class="<?php echo ($active == 'blog')? 'active' : '' ?>"> 
                          <a href="<?php base_url('blog') ?>" class="m-link text-center">Blog</a> <span class="arrow-"></span>
                          
                        </li>
                        <li class="<?php echo ($active == 'contato')? 'active' : '' ?>"> 
                          <a href="<?php base_url('contacto') ?>" class="m-link text-center">Contacto</a> <span class="arrow-"></span>
                          
                        </li>

                     <?php     }?>

                      

                     

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end menu--> 
      
    </div>