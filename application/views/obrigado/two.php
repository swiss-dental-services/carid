
    <!--================1ra sesao =================-->
          <style>
            .neverShowMe {
                display:none!important
            }
            svg {
                width: 142px;
                height: 142px;
                display: block;
                margin: 3vh auto 4vh;
            }
            #thank {
                background-image: url(./assets/img/obrigado/mascara-slide.png), url(./assets/img/obrigado/doutor-implantologia.png), url(./assets/img/obrigado/red.png);
                background-size: 100%, 313px, cover;
                background-position: bottom, 13% bottom, top;
                background-repeat: no-repeat, no-repeat, no-repeat; 
                height: 100vh;
            }

            b {
                font-weight: bold;
            }

            #thank .alignT {
                width: 60%;
                margin: 0 20%;
                padding: 20vh 0 0;
                height: unset;
            }
            #thank .firstText {
                text-align: center;
                height: 60px;
                color: #FFFFFF;
                font-family: "Heebo", sans-serif;
                font-size: 35px;
                line-height: 45px;
                font-weight: 300;
            }
            #thank .mediumText {
                color: #FFFFFF;
                font-family: "Heebo", sans-serif;
                font-size: 35px;
                font-weight: 300;
                line-height: 45px;
                text-align: center;
                margin-bottom: 15px;
            }
            #thank .backText {
                color: #FFFFFF;
                font-family: "Heebo", sans-serif;
                font-weight: 300;
                font-size: 17px;
                line-height: 28px;
                text-align: center;
                margin: 0;
            }
            #thank .button-btn {
                text-align: center;
                width: 209px;
                margin: auto;
                margin-top: 10px;
                /*border-radius: 50% !important;*/
            }
            #thank button {
                margin-top: 50px;
                min-width: 209px;
                border-radius: 32px;
                background-color: #FFFFFF;
                color: #DB0707;
                font-family: "Heebo", sans-serif;
                font-size: 16px;
                font-weight: bold;
                letter-spacing: -0.29px;
                line-height: 19px;
                border: 0px solid #FFFFFF !important;
                box-sizing: border-box;
                padding: 15px 0 15px 0;
                text-transform: uppercase;
                position: relative;
                z-index: 9999;
                outline: none;
                overflow: hidden;
                cursor: pointer;
            }
            #thank button:after {
                content: '';
                position: absolute;
                left: -100%;
                top: 0;
                width: 100%;
                height: 100%;
                background: #DB0707;
                transition: all 0.3s;
                color: white;
                z-index: -1;
                border-radius: inherit;
            }
            #thank button:not(.btn-blue):hover {
                color: white !important;
                border: 0px solid #DB0707 !important;
                transition: border 0.1s linear 0.3s;
            }
            #thank button:not(.btn-blue):hover:after {
                left: 0;
                transition: left 0.3s, border 0s linear 0.5s;
                color: white;
            }
            @media (max-width: 699px){
                #thank {
                    background-size: 100%, 0, cover;
                    background-position: bottom, 10% bottom, top;
                }

                #thank .alignT {
                    width: 90%;
                    margin: 0 5%;
                    padding: 150px 0 0;
                }
                svg {
                    width: 72px;
                    height: 72px;
                }
                #thank .firstText {
                    height: unset;
                    font-size: 21px;
                    line-height: 28px;
                }
                #thank .mediumText {
                    font-size: 21px;
                    line-height: 28px;
                    margin-bottom: 10px;
                }
                


            }
          </style>
        <section class="white_cl">
            <div class="" style="/*padding: 89px 0px 200px*/"><!--clase que estaba aqui container  200px 0px 200px-->
                
                <article id="thank">

                    <div class="alignT">

                        <svg viewBox="9 0 100 90" class="animate">
                            <filter id="dropshadow" height="100%">
                                <feGaussianBlur in="SourceAlpha" stdDeviation="3" result="blur"/>
                                <feFlood flood-color="white" flood-opacity="0.8" result="color"/>
                                <feComposite in="color" in2="blur" operator="in" result="blur"/>
                                <feMerge> 
                                <feMergeNode/>
                                <feMergeNode in="SourceGraphic"/>
                                </feMerge>
                            </filter>
                            <circle cx="58" cy="46" r="46.5" fill="none" stroke="white" stroke-width="5"/>
                            <path d="M67,93 A46.5,46.5 0,1,0 7,32 L43,67 L88,19" fill="none" stroke="white" stroke-width="8" stroke-linecap="" stroke-dasharray="80 1000" stroke-dashoffset="-220"  style="filter:url(#dropshadow)"/>
                        </svg>
                        
                        <h1 class="firstText">Obrigado pelo seu interesse.</h1>
                        <h2 class="mediumText">O seu registo foi <b>submetido com sucesso.</b></h2>
                        <h4 class="backText">Brevemente será contactado pela nossa equipe.</h4>
                        <!--<h4 class="backText">Volte a sorrir sem medo, com implantes dentários!</h4>-->
                    </div>

                    <div class="button-btn">
                        <a href="<?php base_url('') ?>">
                            <button>Home</button>
                        </a>
                    </div>

                </article>
               
            </div>

        </section>  
            
    <!--================1ra sesao  =================-->