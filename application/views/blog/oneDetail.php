<section class="sec-padding">
      <div class="container">
        <div class="row">

          <div class="col-md-8 col-sm-12 col-xs-12" id="listado">


            <?php echo getOneNew($post_id,' 

            <h3 class="text-center mt-mobil-blog-det">{{title}}</h3>
            <div class="padding-blog-details">
              {{details}}
            </div>

          '); ?>   

           

            <!--<h2 class="show-sem-cat">OPS, DESCULPE NÃO TEMOS POTS PARA ESSA CATEGORIA!!!!</h2>

           NAV BUTTONS BLOG 
            <div class="bg2-featurebox-3 text-center bm-50-mobile" style="display: flex; justify-content: space-around;" id="pagination-blog">
              <a href="javascript: void(0)" class="btn-2 btn-red btn-2-border" id="prev">Anterior</a>
              <a href="javascript: void(0)" class="btn-2 btn-red btn-2-border" id="next">Próximo</a>
            </div>-->
            
          </div>
          <!--end left col main--> 
          
          <div class="col-md-4 col-sm-12 col-xs-12 section-white">
              <?php
                echo funGetSlide('blog_one','','','

                <div class="bg2-right-col-item-holder"> <img src="{{img}}" alt="" class="img-responsive"/> <br/>
                  <h5 class="dosis uppercase bg2-right-col-item-title">{{title}}</h5>
                  <p class="font-size-text">{{text}} </p>
                  <br/>
                  <ul class="social-links">
                    <li><a target="_blank" href="https://www.facebook.com/carid"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" class="twitter" href="javascript: void(0)"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/caridportugal/"><i class="fa fa-instagram"></i></a></li>
                    <li><a target="_blank" href="javascript: void(0)"><i class="fa fa-youtube"></i></a></li>
                  </ul>
                </div>
                <!--end item holder-->
                
                ');
              ?>
              
                           
              <div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title"><?=blog_text_category?></h5>
                <ul class="bg2-rightcol-links">
                  <?php
                    echo funGetSlide('blog_categorias-','','','
                      <li><a class="font-size-text" href="{{subtitle}}">{{title}}</a></li>
                    ');
                  ?>
                  <li><a class="font-size-text" href="<?php base_url('blog') ?>">Todas</a></li>
                  <li><a class="font-size-text" href="<?php base_url('blog') ?>?category=Sobre o CARID">Sobre o CARID</a></li>
                  <li><a class="font-size-text" href="<?php base_url('blog') ?>?category=Sobre os Tratamentos">Sobre os Tratamentos</a></li>
                  <li><a class="font-size-text" href="<?php base_url('blog') ?>?category=Mitos e Verdades">Mitos e verdades</a></li>                 
                  
                </ul>
              </div>
              <!--end item holder-->
              
              <div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title"><?=blog_text_post?></h5>

                <?= getNews('            
                  
                  <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div class="imgbox-small left"> <img src="{{image}}" alt="" class="img-responsive"/></div>
                    <div class="text-box-right">
                      <h6 class="uppercase nopadding dosis"><a href="{{href}}" class="text-hover-gyellow">{{title}}</a></h6>
                      <div class="blog-post-info padding-top-1"> <span> {{publishDate}}</span> </div>
                    </div>
                  </div>
                  <!--end post-->
                  
                  <!--<div class="divider-line solid light margin"></div>-->
                  <div class="divisor"></div>',4); 
                ?>


                

              </div>
             
              
            </div>
            <!--end right col-->
            
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->