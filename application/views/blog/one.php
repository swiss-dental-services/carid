
    <!--================1ra sesao =================-->
    <?php

        $cat = isset($_GET['category']) ? $_GET['category'] : null;

    ?>  



    
           
<section class="sec-padding">
      <div class="container">
        <div class="row">

          <div class="col-md-8 col-sm-12 col-xs-12" id="listado">


            <?= getNews('
              
              <div class="bg2-featurebox-3 lista">
                <div class="img-box">
                  <div class="postdate-box">
                    <div class="blog-post-info"> <span><i class="fa fa-user"></i> By {{editor}}</span> </div>
                  </div>
                  <img src="{{image}}" alt="" class="img-responsive wow animated fadeInUp" data-wow-delay="0.0s"/> </div>
                <div class="postinfo-box">
                  <h4 class="dosis uppercase title wow animated fadeInUp" data-wow-delay="0.2s"><a href="{{href}}">{{title}}</a></h4>
                  <div class="blog-post-info mobile wow animated fadeInUpBig" data-wow-delay="0.4s"><span><i class="fa fa-file"></i> {{category}}</span> <span><i class="fa fa-calendar"></i> {{publishDate}}</span> <span><i class="fa fa-user"></i> By {{editor}}</span>  </div>
                  <br/>
                  <p class="font-size-text wow animated fadeInUp" data-wow-delay="0.6s">{{subTitle}}</p>
                  <br/>
                  <br/>
                  <div class="wow animated fadeInUp" data-wow-delay="0.8s">
                    <a href="{{href}}" class="less-pad btn-2 btn-red btn-2-border">Saiba mais</a> </div>
                  </div>
              </div>', null, null, null, $cat); ?>



            <!--<div class="bg2-featurebox-3 lista">
              <div class="img-box">
                <div class="postdate-box">
                  <div class="blog-post-info"> <span><i class="fa fa-user"></i> By Benjamin</span> </div>
                </div>
                <img src="<?php /*base_url('bg2-20.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/> </div>
              <div class="postinfo-box">
                <h4 class="dosis uppercase title"><a href="#">Aliquam Rhoncus amet Maecenas sed nisl 1</a></h4>
                <div class="blog-post-info mobile"><span><i class="fa fa-user"></i> By Comments</span> <span><i class="fa fa-calendar"></i> 20/05/2021</span> <span><i class="fa fa-file"></i> Categoria</span> </div>
                <br/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet ligula. Etiam sit amet dolor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Nullam tellus diam volutpat laoreet vel bibendum in nibh Donec elit lectus pharetra quis vulputate in lobortis at mi Donec libero. </p>
                <br/>
                <br/>
                <a href="#" class="less-pad btn-2 btn-red btn-2-border">Read More</a> </div>
            </div>
            end post item
            
            <div class="bg2-featurebox-3 lista">
              <div class="img-box">
                <div class="postdate-box">
                  <div class="blog-post-info"> <span><i class="fa fa-user"></i> By Benjamin</span> <span><i class="fa fa-comments-o"></i> 15 Comments</span> </div>
                </div>
                <img src="<?php /*base_url('bg2-20.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/> </div>
              <div class="postinfo-box">
                <h4 class="dosis uppercase title"><a href="#">Aliquam Rhoncus amet Maecenas sed nisl 2</a></h4>
                <div class="blog-post-info"><span><i class="fa fa-comments-o"></i> 15 Comments</span> <span><i class="fa fa-folder"></i> Business/agro</span><span><i class="fa fa-thumbs-up"></i> 256 Likes</span> </div>
                <br/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet ligula. Etiam sit amet dolor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Nullam tellus diam volutpat laoreet vel bibendum in nibh Donec elit lectus pharetra quis vulputate in lobortis at mi Donec libero. </p>
                <br/>
                <br/>
                <a class="btn btn-dark-3 btn-small uppercase" href="#">Read more</a> </div>
            </div>
            end post item
            
            <div class="bg2-featurebox-3 lista">
              <div class="img-box">
                <div class="postdate-box">
                  <div class="blog-post-info"> <span><i class="fa fa-user"></i> By Benjamin</span> <span><i class="fa fa-comments-o"></i> 15 Comments</span> </div>
                </div>
                <img src="<?php /*base_url('bg2-20.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/> </div>
              <div class="postinfo-box">
                <h4 class="dosis uppercase title"><a href="#">Aliquam Rhoncus amet Maecenas sed nisl 3</a></h4>
                <div class="blog-post-info"><span><i class="fa fa-comments-o"></i> 15 Comments</span> <span><i class="fa fa-folder"></i> Business/agro</span><span><i class="fa fa-thumbs-up"></i> 256 Likes</span> </div>
                <br/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet ligula. Etiam sit amet dolor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Nullam tellus diam volutpat laoreet vel bibendum in nibh Donec elit lectus pharetra quis vulputate in lobortis at mi Donec libero. </p>
                <br/>
                <br/>
                <a class="btn btn-dark-3 btn-small uppercase" href="#">Read more</a> </div>
            </div>
            end post item
            
            <div class="bg2-featurebox-3 lista">
              <div class="img-box">
                <div class="postdate-box">
                  <div class="blog-post-info"> <span><i class="fa fa-user"></i> By Benjamin</span> <span><i class="fa fa-comments-o"></i> 15 Comments</span> </div>
                </div>
                <img src="<?php /*base_url('bg2-20.jpg','img/custon')*/ ?>" alt="" class="img-responsive"/> </div>
              <div class="postinfo-box">
                <h4 class="dosis uppercase title"><a href="#">Aliquam Rhoncus amet Maecenas sed nisl 4</a></h4>
                <div class="blog-post-info"><span><i class="fa fa-comments-o"></i> 15 Comments</span> <span><i class="fa fa-folder"></i> Business/agro</span><span><i class="fa fa-thumbs-up"></i> 256 Likes</span> </div>
                <br/>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet ligula. Etiam sit amet dolor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Nullam tellus diam volutpat laoreet vel bibendum in nibh Donec elit lectus pharetra quis vulputate in lobortis at mi Donec libero. </p>
                <br/>
                <br/>
                <a class="btn btn-dark-3 btn-small uppercase" href="#">Read more</a> </div>
            </div>
            end post item--> 

            <h2 class="show-sem-cat">OPS, DESCULPE NÃO TEMOS POTS PARA ESSA CATEGORIA!!!!</h2>

           <!--NAV BUTTONS BLOG--> 
            <div class="bg2-featurebox-3 text-center bm-50-mobile" style="display: flex; justify-content: space-around;" id="pagination-blog">
              <a href="javascript: void(0)" class="btn-2 btn-red btn-2-border" id="prev">Anterior</a>
              <a href="javascript: void(0)" class="btn-2 btn-red btn-2-border" id="next">Próximo</a>
            </div>
            
          </div>
          <!--end left col main--> 
          
          <div class="col-md-4 col-sm-12 col-xs-12 section-white">
            <?php
              echo funGetSlide('blog_one','','','

              <div class="bg2-right-col-item-holder"> <img src="{{img}}" alt="" class="img-responsive"/> <br/>
                <h5 class="dosis uppercase bg2-right-col-item-title">{{title}}</h5>
                <p class="font-size-text">{{text}} </p>
                <br/>
                <ul class="social-links">
                  <li><a target="_blank" href="https://www.facebook.com/carid"><i class="fa fa-facebook"></i></a></li>
                  <li><a target="_blank" class="twitter" href="javascript: void(0)"><i class="fa fa-twitter"></i></a></li>
                  <li><a target="_blank" href="https://www.instagram.com/caridportugal/"><i class="fa fa-instagram"></i></a></li>
                  <li><a target="_blank" href="javascript: void(0)"><i class="fa fa-youtube"></i></a></li>
                </ul>
              </div>
              <!--end item holder-->
              
              ');
            ?>
              
              <!--<div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title">Search</h5>
                <input type="text" name="name" class="bg2-news-letter" value="Search" maxlength="100" />
              </div>
              end item holder-->
              
              <div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title"><?=blog_text_category?></h5>
                <ul class="bg2-rightcol-links">
                  <?php
                    echo funGetSlide('blog_categorias','','','
                      <li><a class="font-size-text" href="{{subtitle}}">{{title}}</a></li>
                    ');
                  ?>
                  <!--<li><a href="#">Business</a></li>
                  <li><a href="#">Education</a></li>
                  <li><a class="active-" href="#">Travel</a></li>
                  <li><a href="#">Creative</a></li>
                  <li><a href="#">Wedding</a></li>
                  <li><a href="#">Restaurant</a></li>-->
                </ul>
              </div>
              <!--end item holder-->
              
              <div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title"><?=blog_text_post?></h5>

                <?= getNews('            
                  
                  <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div class="imgbox-small left"> <img src="{{image}}" alt="" class="img-responsive"/></div>
                    <div class="text-box-right">
                      <h6 class="uppercase nopadding dosis"><a href="{{href}}" class="text-hover-gyellow">{{title}}</a></h6>
                      <div class="blog-post-info padding-top-1"> <span> {{publishDate}}</span> </div>
                    </div>
                  </div>
                  <!--end post-->
                  
                  <!--<div class="divider-line solid light margin"></div>-->
                  <div class="divisor"></div>',4); 
                ?>


                <!--<div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="imgbox-small left"> <img src="http://placehold.it/250x250" alt="" class="img-responsive"/></div>
                  <div class="text-box-right">
                    <h6 class="uppercase nopadding dosis"><a href="#" class="text-hover-gyellow">Nullam turpis Cras dapibusscing</a></h6>
                    <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> </div>
                  </div>
                </div>
                end post
                
                
                <div class="divisor"></div>

                <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="imgbox-small left"> <img src="http://placehold.it/250x250" alt="" class="img-responsive"/></div>
                  <div class="text-box-right">
                    <h6 class="uppercase nopadding dosis"><a href="#" class="text-hover-gyellow">Nullam turpis Cras dapibusscing</a></h6>
                    <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> </div>
                  </div>
                </div>
                              
                
                <div class="divisor"></div>

                <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="imgbox-small left"> <img src="http://placehold.it/250x250" alt="" class="img-responsive"/></div>
                  <div class="text-box-right">
                    <h6 class="uppercase nopadding dosis"><a href="#" class="text-hover-gyellow">Nullam turpis Cras dapibusscing</a></h6>
                    <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> </div>
                  </div>
                </div>
                
                <div class="divisor"></div>-->

              </div>
              <!--end item holder-->
              
              <!--<div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title">Tags</h5>
                <ul class="bg2-tags">
                  <li><a href="#">Animation</a></li>
                  <li><a href="#">Art</a></li>
                  <li><a href="#">UI Design</a></li>
                  <li><a href="#">Photography</a></li>
                  <li><a class="active" href="#">Design</a></li>
                  <li><a href="#">Art</a></li>
                  <li><a href="#">Responsive</a></li>
                  <li><a href="#">Develop</a></li>
                </ul>
              </div>
              end item holder-->
              
              <!--<div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title">Newsletter</h5>
                <input type="text" name="name" class="bg2-news-letter" value="Enter your e-mail" maxlength="100" />
              </div>
              end item holder-->
              
              <!--<div class="bg2-right-col-item-holder">
                <h5 class="dosis uppercase bg2-right-col-item-title">Featured Works</h5>
                <ul class="sidebar-works">
                  <li><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                  <li><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                  <li><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                  <li><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                  <li><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                  <li class="last"><a href="#"><img src="http://placehold.it/600x600" alt=""/></a></li>
                </ul>
              </div>
              end item holder--> 
              
            </div>
            <!--end right col-->
            
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->
    

                        