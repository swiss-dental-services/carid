<!--================Footer Area =================-->
	
<section class="section-dark- bg-footer sec-padding">
      <div class="container ">
        <div class="row"> <br/>
          <div class="col-md-3 col-sm-12 colmargin clearfix margin-bottom">
            <div class="fo-map">
              <div class="footer-logo"><img src="<?php base_url('Logo-Carid-03.svg','img/custon') ?>" alt="" width="90%"/></div>
              <p>Rua Brigadeiro Nunes da Ponte, <br> 114 2º andar, Ed. Bessa Leite</p>
             
              <address>
              <strong></strong>
              <br>
              Office 4150-036 Porto
              <br>
              Clínica +351 800200208
              <br>
              Escritório +351 226093654
              </address>
              
              <span>info@carid.pt</span><br>
        
            </div>
          </div>
          <!--end item-->
          
          <div class="col-md-3 col-sm-12 col-xs-12 clearfix margin-bottom">
            <h5 class="text-white uppercase less-mar3 font-weight-5">Navegação</h5>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip gyellow" style="background-color: #fff !important;"></div>
            <ul class="footer-quick-links-4">
              <li><a href="<?php base_url('sobre') ?>"><i class="fa fa-angle-right"></i> Sobre</a></li>
              <li><a href="<?php base_url('condicoes-financiamento') ?>"><i class="fa fa-angle-right"></i> Condições Financiamento</a></li>
              <li><a href="<?php base_url('reabilitacao-oral') ?>"><i class="fa fa-angle-right"></i> Reabilitação Oral</a></li>
              <li><a href="<?php base_url('casos-clinicos') ?>"><i class="fa fa-angle-right"></i> Casos Clínicos</a></li>
              <li><a href="<?php base_url('campanhas') ?>"><i class="fa fa-angle-right"></i> Campanhas</a></li>
              <li><a href="<?php base_url('blog') ?>"><i class="fa fa-angle-right"></i> Blog</a></li>
              <li><a href="<?php base_url('contacto') ?>"><i class="fa fa-angle-right"></i> Contacto</a></li>
            </ul>
          </div>
          <!--end item-->
          
          <div class="col-md-3 col-sm-12 col-xs-12 clearfix margin-bottom">
            <h5 class="text-white uppercase less-mar3 font-weight-5">Ligações Úteis</h5>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip gyellow" style="background-color: #fff !important;"></div>
            <ul class="footer-quick-links-4">
              <li><a href="<?php base_url('contacto') ?>"><i class="fa fa-angle-right"></i> FAQ´s</a></li>
              <li><a href="<?php base_url('termos-condicoes') ?>"><i class="fa fa-angle-right"></i> Termos e Condições | Privacidade</a></li>              
            </ul>
          </div>
          <!--end item-->
          
          <div class="col-md-3 col-sm-12 col-xs-12 clearfix margin-bottom">
            <h5 class="text-white uppercase less-mar3 font-weight-5">ligue já</h5>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip gyellow" style="background-color: #fff !important;"></div>
            <div class="fo-map">
              <p><strong>Número Grátis</strong> <br> <span style="color: #8b001d; font-size: 25px; font-weight: bold;">800 200 208</span> </p>
                            
            </div>
          </div>
          </div>
          <!--end item-->
          
          <div class="clearfix"></div>
          <div class="col-divider-margin-4"></div>
          <div class="divider-line solid light opacity-1"></div>
          <div class="col-divider-margin-4"></div>
          
          <div class="col-md-4 col-sm-12 colmargin clearfix margin-bottom">
            <div class="fo-map">
              <div class="footer-logo desktop-logo" style="margin-bottom: 0px;"> <a href="https://swissdentalservices.com/" target="_blank"><img src="<?php base_url('logo_swiss_dental_services-01.png','img/custon') ?>" alt="" width="90%"/></a> </div>
              <div class="footer-title-bottomstrip gyellow" style="background-color: #fff !important;"></div>

              <p>Swiss Dental Services. Todos os direitos reservados.</p>

              <p>SDS - Portugal Sociedade Suiça de Serviços Odontológicos, Lda.</p>

              <p>Rua Santos Pousada, 441 - Sala 106, 4000-486 Porto</p>

                           
                      
            </div>
          </div>
          <!--end item-->
          
          <div class="col-md-4 col-sm-12 col-xs-12 clearfix margin-bottom">
            <h5 class="text-white uppercase less-mar3 font-weight-5">Orgãos reguladores</h5>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip gyellow" style="background-color: #fff !important;"></div>
            <ul class="footer-quick-links-4">
              <li><a href="https://www.omd.pt/" target="_blank"><i class="fa fa-angle-right"></i> OMD Ordem Médicos Dentistas</a></li>
              <li><a href="https://www.ers.pt/pt/" target="_blank"><i class="fa fa-angle-right"></i> ERS Entidade Reguladora da Saúde</a></li>
              <li><a href="https://www.arsnorte.min-saude.pt/" target="_blank"><i class="fa fa-angle-right"></i> ARS Administração Regional da Saúde</a></li>
              <li><a href="https://www.dgs.pt/" target="_blank"><i class="fa fa-angle-right"></i> DGS Direção Geral de Saúde</a></li>
              <li><a href="https://www.sns.gov.pt/institucional/ministerio-da-saude/" target="_blank"><i class="fa fa-angle-right"></i> Ministério da Saúde</a></li>
              <li><a href="https://www.deco.proteste.pt/" target="_blank"><i class="fa fa-angle-right"></i> DECO</a></li>
            </ul>
          </div>
          <!--end item-->         
          
          
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!--end section-->
    
    <section class="sec-moreless-padding">
      <div class="container">
        <div class="row">
          <div class="fo-copyright-holder text-center">
            <div class="social-iconbox">
              <div class="side-shape1"><img src="<?php base_url('fo-icon-box-shape1.png','img/custon') ?>" alt=""/></div>
              <div class="side-shape1 right-icon"><img src="<?php base_url('fo-icon-box-shape2.png','img/custon') ?>" alt=""/></div>
              <ul class="sc-icons">
                <li><a target="_blank" class="redes-icone-footer" href="https://www.facebook.com/carid"><i class="fa fa-facebook"></i></a></li>
                <li><a target="_blank" class="redes-icone-footer" href="javascript: void(0)"><i class="fa fa-twitter"></i></a></li>
                <li><a target="_blank" class="redes-icone-footer" href="https://www.instagram.com/caridportugal/"><i class="fa fa-instagram"></i></a></li>
                <li><a target="_blank" class="redes-icone-footer" href="javascript: void(0)"><i class="fa fa-youtube"></i></a></li>
              </ul>
            </div>
            CARID © <?php echo date("Y"); ?> | Todos os direitos reservados. </div>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>  
    
<a href="#" class="scrollup" style="background: #f50f40 url(<?php base_url('scroll-top-arrow.png','img/custon') ?>) no-repeat 15px 16px; border-radius: 50%;"></a><!-- end scroll to top of the page--> 