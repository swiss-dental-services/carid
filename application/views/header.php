
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="google-site-verification" content="ISgLojcPdilsaCmi1GPRfqTeSQWr1REMbBfOLiL6vj4" />
        
        <link rel="apple-touch-icon" sizes="180x180" href="<?php base_url('apple-touch-icon.png','img/favicon') ?>">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php base_url('favicon-32x32.png','img/favicon') ?>">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php base_url('favicon-16x16.png','img/favicon') ?>">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>CARID </title>



        <!--  GOOGLE FONT --> 
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">

        <!-- BOOTSTRAP -->
        <link href="<?php base_url('bootstrap.min.css','css') ?>" rel="stylesheet"> 


        <!-- Template's stylesheets -->

                
        <link href="<?php base_url('screen.css','css') ?>" rel="stylesheet">    
        <link href="<?php base_url('theme-default.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('screen2.css','css') ?>" rel="stylesheet">    
        <link href="<?php base_url('corporate.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('font-awesome.min.css','css') ?>" rel="stylesheet">  

        <link href="<?php base_url('simple-line-icons.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('et-line-font.css','css') ?>" rel="stylesheet">    

        <link href="<?php base_url('settings.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('layers.css','css') ?>" rel="stylesheet">  

        <link href="<?php base_url('navigation.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('owl.carousel.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('owl.theme.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('cubeportfolio.min.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('smk-accordion.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('ytplayer.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('responsive-tabs.css','css') ?>" rel="stylesheet">  

        <link href="<?php base_url('style.css','css') ?>" rel="stylesheet">
        
        <!-- Style Customizer's stylesheets -->
        <link href="<?php base_url('spectrum.css','css') ?>" rel="stylesheet">
        <link href="<?php base_url('style-customizer.css','css') ?>" rel="stylesheet">
        <link href="<?php base_url('skin.less','css') ?>" rel="stylesheet/less" type="text/css">

        
        
		<!-- sweetalerts2 -->
        <link href="<?php base_url('sweetalert2.css','css') ?>" rel="stylesheet"> 
        <!--<link href="<?php /*base_url('sweetalert2.min.css','css')*/ ?>" rel="stylesheet">--> 

        <!-- Custon css --> 
        <link href="<?php base_url('custon.css','css') ?>" rel="stylesheet">

        <!-- ANIMATE --> 
        <link href="<?php base_url('animate.css','css') ?>" rel="stylesheet">

        <!-- MAGNIFIC POPUP --> 
        <link href="<?php base_url('magnific-popup.css','css') ?>" rel="stylesheet">


        <link rel="stylesheet" type="text/css" href="<?php base_url('basic_form.css','css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php base_url('form_exemplo.css','css') ?>">
        <link href="<?php base_url('form-gets-leads.css','css') ?>" rel="stylesheet">


        <link href="<?php base_url('blog2.css','css') ?>" rel="stylesheet">

        <link href="<?php base_url('shortcodes.css','css') ?>" rel="stylesheet">


        <!-- ICONS ELEGANT FONT & FONT AWESOME & LINEA ICONS   
        <link href="<?php /*base_url('icons-fonts.css','css')*/ ?>" rel="stylesheet">
        <link href="<?php /*base_url('font-awesome.css','css')*/ ?>" rel="stylesheet">--> 

        <!--  CSS THEME 
        <link href="<?php /*base_url('style.css','css')*/ ?>" rel="stylesheet">--> 
        
        

        <!-- Icon css link 
        <link href="<?php /*base_url('ionicons.min.css','css')*/ ?>" rel="stylesheet">-->

        <!-- FORMULARIOS 
        <link href="<?php /*base_url('formularios.css','css')*/ ?>" rel="stylesheet">-->

        <!-- IMAGES COMPARE 
        <link href="<?php /*base_url('twentytwenty.css','css')*/ ?>" rel="stylesheet">-->

        
    </head>
    
        
       