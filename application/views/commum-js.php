<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        
        <!-- jQuery  -->
        <script src="<?php base_url('jquery.js','js') ?>"></script>

        <!-- jQuery min 
        <script src="<?php /*base_url('jquery.min.js','js')*/ ?>"></script> -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php base_url('bootstrap.min.js','js') ?>"></script>   

        <!--Sweet Alert   -->
        <script src="<?php base_url('sweetalert2.all.min.js','js') ?>"></script>


        <script src="<?php base_url('spectrum.js','js') ?>"></script>
        <script src="<?php base_url('less.min.js','js') ?>"></script>
        <script src="<?php base_url('style-customizer.js','js') ?>"></script>

        <!-- Template scripts --> 
        <script src="<?php base_url('main.js','js') ?>"></script>
        <script src="<?php base_url('owl.carousel.js','js') ?>"></script>
        <script src="<?php base_url('custom.js','js') ?>"></script>
        <script src="<?php base_url('jquery.mb.YTPlayer.js','js') ?>"></script>
        <script src="<?php base_url('elementvideo-custom.js','js') ?>"></script>
        <script src="<?php base_url('play-pause-btn.js','js') ?>"></script>
        <script src="<?php base_url('jquery.cubeportfolio.min.js','js') ?>"></script>
        <script src="<?php base_url('main-mosaic3.js','js') ?>"></script>
        <script src="<?php base_url('smk-accordion.js','js') ?>"></script>
        <script src="<?php base_url('custom2.js','js') ?>"></script>
        <script src="<?php base_url('responsive-tabs.min.js','js') ?>"></script>

        <!-- REVOLUTION JS FILES --> 
        <script src="<?php base_url('jquery.themepunch.tools.min.js','js') ?>"></script>
        <script src="<?php base_url('jquery.themepunch.revolution.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.video.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.slideanims.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.layeranimation.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.kenburn.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.navigation.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.parallax.min.js','js') ?>"></script>
        <script src="<?php base_url('revolution.extension.actions.min.js','js') ?>"></script>


        <script src="<?php base_url('wow.min.js','js') ?>"></script>

        <script src="<?php base_url('jquery.mask.min.js','js') ?>"></script>

        <!-- MAGNIFIC POPUP -->
        <script src="<?php base_url('jquery.magnific-popup.js','js') ?>"></script>

        <!-- FORMULARIOS GET LEADS -->
        <script src="<?php base_url('formularios_get_leads.js','js') ?>" ></script>


        <script type="text/javascript">

          function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (30*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
          }

          function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function reloadCookie(_var) {
                setCookie("lang",_var,30);
                setTimeout(() => {
                    location.reload();
                }, 500);
            }      

            
            function reload(){

                
                $('html,body').animate({scrollTop:0}, 100);
                window.location.reload();
            }

         
        
        function onlynumber(evt) {
             var theEvent = evt || window.event;
             var key = theEvent.keyCode || theEvent.which;
             key = String.fromCharCode( key );
             
             var regex = /^[0-9.]+$/;
             if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
             }
        }           	
        	
        </script>

        <script src="https://webserver.swissdentalservices.com/formularios/form-generator.js"></script>

        <script>
        /**
        *
        * @param {String} id Id do formulário
        * @param {String} cta Texto para o CTA do formulário.
        * @param {String} thanks Thank Page do site, colocar FALSE caso não queira utilizar.
        * @param {Array} extra Array de objetos com campos extras que serão adicionados ao formulário.
        * @param {Array} data Array de objetos com cada campo que existirá no formulário.
        */
        var existForm = document.getElementById("form-site-carid");
        if( existForm ){

            var fid = "form-site-carid";
            var fcta = "Quero uma avaliação sem custos";//"Saber Mais";
            var ftks = "<?php base_url('thank-page') ?>";
            form(fid, fcta, ftks, [{ type: "hidden", name: "origem", value: "LP" }],[
            { type: "form", name: fid, css: "testeform", validate: "true"},
            { type: "hidden", name: "id", value: fid },


            { type: "text", name: "name", value: null, options: null, required: true, css: null, title: 'Nome', placeholder: 'Nome *', pattern: false },
            { type: "email", name: "email", value: null, options: null, required: true, css: null, title: 'E-mail', placeholder:'E-mail *' },
            { type: "select", name: "q1", value: null,
            options: {
            "": "Selecione",
            "Braga": "Braga",
            "Coimbra": "Coimbra",
            "Leiria": "Leiria",
            "Lisboa": "Lisboa",
            "Porto": "Porto",
            "Portimão": "Portimão",
            "Santarém": "Santarém",
            "Vila Real": "Vila Real",
            "Viseu": "Viseu",
            "Londres": "Londres",
            "Paris": "Paris"
            },
            required: false, css: null, title: "CLÍNICA MAIS PERTO DE SI"
            }, {
            type: "phone" ,
            name: "phone",
            prefix: { "PT": "+351", "AT": "+43","BE": "+32", "CH": "+41","DE": "+49", "DK": "+45","ES": "+34","FR": "+33", "GB": "+44","IE": "+353","IT": "+39","LU": "+352","NL": "+31", "NO": "+47","SE": "+46"},
            required: true,
            css: null,
            title: "Telefone",
            placeholder:"*"
            }, {
            type: "select", name: 'melhor_altura_para_contacto', value: null, options: {"": "Selecione", "manha": "Manhã", "tarde": "Tarde", "noite": "Noite"}, required: false, title: "MELHOR ALTURA PARA CONTACTO"
            },
            { type: "checkbox", name: "termos", value: "<a href='termos-condicoes' target='blank'>Li e aceito os Termos e Condições</a>", required: true, css: "notice"},
            { type: "submit", name: "", value: fcta , css: null, id: "agende-fale-connosco" }

            ]).start();

        }
        
        </script>

               
            
        <script type="text/javascript">
          
          var estado = document.getElementById("form-home__phone");
          /*if( existForm ){
              estado.onkeypress = function(evt){


                 var theEvent = evt || window.event;
                 var key = theEvent.keyCode || theEvent.which;
                 key = String.fromCharCode( key );
                 
                 var regex = /^[0-9.]+$/;
                 if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                 }

                
              };
          }*/

          $("#form-home__phone").mask("(99) 99999-9999");


          var existFormSuporte = document.getElementById("form_suporte_fale_connosco");
          if( existFormSuporte ){

              var estado_2 = document.getElementById("telefone_fale_connosco");
              estado_2.onkeypress = function(evt){


                 var theEvent = evt || window.event;
                 var key = theEvent.keyCode || theEvent.which;
                 key = String.fromCharCode( key );
                 
                 var regex = /^[0-9.]+$/;
                 if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                 }

                
              };

              $("#telefone_fale_connosco").mask("(99) 99999-9999");
          }


          
         
          
        </script>

        <script type="text/javascript">
  var tpj=jQuery;     
  var revapi4;
  tpj(document).ready(function() {
  if(tpj("#rev_slider").revolution == undefined){
  revslider_showDoubleJqueryError("#rev_slider");
  }else{
    revapi4 = tpj("#rev_slider").show().revolution({
    sliderType:"standard",
    jsFileLocation:"js/revolution-slider/js/",
    sliderLayout:"auto",
    dottedOverlay:"none",
    delay:9000,
    navigation: {
    keyboardNavigation:"off",
    keyboard_direction: "horizontal",
    mouseScrollNavigation:"off",
    onHoverStop:"off",
    arrows: {
    style:"erinyen",
    enable:true,
    hide_onmobile:true,
    hide_under:778,
    hide_onleave:true,
    hide_delay:200,
    hide_delay_mobile:1200,
    tmp:'',
    left: {
    h_align:"left",
    v_align:"center",
    h_offset:80,
    v_offset:0
    },
    right: {
    h_align:"right",
    v_align:"center",
    h_offset:80,
    v_offset:0
    }
    }
    ,
    touch:{
    touchenabled:"on",
    swipe_threshold: 75,
    swipe_min_touches: 1,
    swipe_direction: "horizontal",
    drag_block_vertical: false
  }
  ,
                    
                    
                    
  },
    viewPort: {
    enable:true,
    outof:"pause",
    visible_area:"80%"
  },
  
  responsiveLevels:[1240,1024,778,480],
  gridwidth:[1240,1024,778,480],
   gridheight: [868,768,1000,950],
  lazyType:"smart",
    parallax: {
    type:"mouse",
    origo:"slidercenter",
    speed:2000,
    levels:[2,3,4,5,6,7,12,16,10,50],
    },
  shadow:0,
  spinner:"off",
  stopLoop:"off",
  stopAfterLoops:-1,
  stopAtSlide:-1,
  shuffle:"off",
  autoHeight:"off",
  hideThumbsOnMobile:"off",
  hideSliderAtLimit:0,
  hideCaptionAtLimit:0,
  hideAllCaptionAtLilmit:0,
  disableProgressBar:"on",
  debugMode:false,
    fallbacks: {
    simplifyAll:"off",
    nextSlideOnWindowFocus:"off",
    disableFocusListener:false,
    }
  });
  }
  }); /*ready*/
</script> 
<script type="text/javascript">
  var tpj = jQuery;

  var revapi280;
  tpj(document).ready(function() {
    if (tpj("#rev_slider_280_1").revolution == undefined) {
      revslider_showDoubleJqueryError("#rev_slider_280_1");
    } else {
      revapi280 = tpj("#rev_slider_280_1").show().revolution({
        sliderType: "hero",
        jsFileLocation: "../../revolution/js/",
        sliderLayout: "auto",
        dottedOverlay: "none",
        delay: 9000,
        navigation: {},
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1000, 1024, 778, 480],
        gridheight: [520, 520, 420, 420],
        lazyType: "none",
        parallax: {
          type:"mouse+scroll",
          origo:"slidercenter",
          speed:2000,
          levels:[1,2,3,20,25,30,35,40,45,50],
          disable_onmobile:"on"
        },
        shadow: 0,
        spinner: "spinner2",
        autoHeight: "off",
        fullScreenAutoWidth: "off",
        fullScreenAlignForce: "off",
        fullScreenOffsetContainer: "",
        fullScreenOffset: "60",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: "off",
          disableFocusListener: false,
        }
      });
    }
  }); /*ready*/
</script> 

<script src="<?php base_url('jflickrfeed.min.js','js') ?>"></script>

<script>
    $('#basicuse').jflickrfeed({
    limit: 6,
    qstrings: {
    id: '133294431@N08'
    },
    itemTemplate: 
    '<li>' +
    '<a href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a>' +
    '</li>'
    });
</script>
              
<script>
  $(window).load(function(){
    setTimeout(function(){

      $('.loader-live').fadeOut();
    },1000);
  })


$(document).ready(function() {

  
    /*scroll to top*/
    $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
    $('.scrollup').fadeIn();
    } else {
    $('.scrollup').fadeOut();
    }
    });
    
    $('.scrollup').on("click",function() {
    $("html, body").animate({
    scrollTop: 0
    }, 500);
    return false;
    
    });

});

$(window).scroll(function (event) {
  var scroll = $(window).scrollTop();
       
   if(scroll>=200){

        $("div a img.img-logo").attr('src', '<?php base_url('Logo-Carid-01.svg','img/custon') ?>');
       

   }else{

        $("div a img.img-logo").attr('src', '<?php base_url('Logo-Carid-03.svg','img/custon') ?>');
   
   }
   
});

/* --------------------------------------------
            SMOOTH SCROLL TO 
          --------------------------------------------- */
          $('a.smooth-scroll[href^="#"]').on('click', function(event) {

            var target = $( $(this).attr('href') );

            if( target.length ) {
              event.preventDefault();
              $('html, body').animate({
                scrollTop: target.offset().top - 80
              }, 600);
            }   

          });


  $('.js-video-play').magnificPopup({
      type: 'iframe',
      removalDelay: 300
    });


</script>
<script>

  new WOW().init();


  jQuery(document).ready(function() {
    function close_accordion_section() {
      jQuery('.accordion .accordion-section-title').removeClass('active');
      jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function(e) {
      // Grab current anchor value
      var currentAttrValue = jQuery(this).attr('href');

      if(jQuery(e.target).is('.active')) {
        close_accordion_section();
      }else {
        close_accordion_section();

        // Add active class to section title
        jQuery(this).addClass('active');
        // Open up the hidden content panel
        jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
      }

      e.preventDefault();
    });

    $(".accordion_head").click(function () {
        if ($('.accordion_body').is(':visible')) {
            $(".accordion_body").slideUp(300);
            $(".plusminus").text('+');
        }
        if ($(this).next(".accordion_body").is(':visible')) {
            $(this).next(".accordion_body").slideUp(300);
            $(this).children(".plusminus").text('+');
        } else {
            $(this).next(".accordion_body").slideDown(300);
            $(this).children(".plusminus").text('-');
        }
    });



  });


</script>


