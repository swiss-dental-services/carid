
    <!--****************************************************** 3ra sessao  ******************************************************************-->        

    <section>
      <div class="divider-line solid light"></div>
      <div class="container">
        <div class="row sec-padding">
          <div class="col-md-6">

            <div class="border-div">
              <a href="https://www.youtube.com/watch?v=1zr1EMONLvU" class="v2 js-video-play"><img class="img-play" src="assets/img/custon/play-01.svg"></a> 
            </div> 
                                   
          </div>
          <!--end item-->
          
          <div class="col-md-6 padding-left-3">
            <div class="col-xs-12 nopadding">
              <div class="sec-title-container less-padding-3 text-left">
                <div class="ce4-title-line-1 align-left" style="background-color: #f50f40;"></div>
                <h4 class="uppercase font-weight-7 less-mar-1"><?=sobre_three__text_1?> <span  style="color: #f50f40;"><?=sobre_three__text_2?></span>  <br> <span  style="color: #f50f40;"><?=sobre_three__text_3?></span> <?=sobre_three__text_4?> </h4>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="clearfix"></div>
            <!--end title-->
            <?php
              echo funGetAdvancedBanners('sobre_three', '

                <p>{{text}}</p>
                <br/>
                <div class="iconlist-2">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow"></i></div>
                  <div class="text">{{subtext}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow"></i></div>
                  <div class="text">{{title}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow"></i></div>
                  <div class="text">{{subtitle}}</div>
                </div>
                <!--end item-->
                
                <div class="iconlist-2">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow"></i></div>
                  <div class="text">{{callTitle}}</div>
                </div>
                <!--end item--> 

                <div class="iconlist-2">
                  <div class="icon dark"><i class="fa fa-arrow-circle-right text-gyellow"></i></div>
                  <div class="text">{{callAction}}</div>
                </div>
                <!--end item--> 
                                      
              ');
            ?>
            
          </div>
          <!--end item--> 
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->