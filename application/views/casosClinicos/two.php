<!--****************************************************** 2da sessao  *************************************************************-->


  <section class="sec-tpadding-2">
  <div class="container-fluid">

    <div class="row">

      <div class="col-xs-12 nopadding wow animated fadeInUp">
        <div class="sec-title-container text-center">
          <div class="ce4-title-line-1" style="background-color: #f50f40;"></div>
          <h4 class="uppercase font-weight-7 less-mar-1 font-size-title"><?=casos_clinicos_text_1 ?></h4>
          <div class="clearfix"></div>
          <p class="by-sub-title font-size-text"><?=casos_clinicos_text_2 ?></p>
        </div>
      </div>
      
    </div>

    <div class="row">
      <div class="col-md-6 nopadding">
        <div class="ce4-feature-box-1- ce4-feature-box-1-custon-2 margin-bottom">
           <div class="img-box-main"> 
             <div class="img-box">
               <img src="<?php base_url('imagem_1_DEP.jpg','img/custon') ?>" alt="" class="img-responsive"/> 

               <div class="btn-video-2">
                <a class="btn-2 btn-red-transparent uppercase js-video-play" href="https://www.youtube.com/watch?v=ab6EYlMy0F4">ASSISTA AO VIDEO </a>                 
               </div>

             </div>

             <div class="clearfix"></div>
             <br/>
             <?php
                echo funGetSlide('casos_clinicos_two_depo_1','','','


             <div class="text text-custon wow animated fadeInUp">
                <p class="font-size-text">{{title}}</p>
                <p class="font-size-text" style="color: #f50f40; margin-top: -12px;">{{subtitle}}</p>
                <p class="font-size-text">{{text}}</p>
             </div>           

                ');
             ?>

           </div>

          </div>
      </div>
      <!--end item-->
      
      <div class="col-md-6 margin-bottom text-center nopadding">
      
      
        <div class="ce4-feature-box-48 height-custon mb-85-neg-mobil">
        
          <div class="text-box-main">
            <div class="text-box box-custon-2">
              <div class="col-xs-12 nopadding">
                <div class="sec-title-container text-center paddin-cust">
                  <br/><br/>                  
                </div>
              </div>
              <div class="clearfix"></div>
              <!--end title--> 
              <div class="btn-video">
               <a class="btn-2 btn-red-transparent uppercase js-video-play" href="https://www.youtube.com/watch?v=H8fMYL5d5tE">ASSISTA AO VIDEO </a>                  
              </div>    
                 
              <div class="clearfix"></div>


            </div>
            <?php
                echo funGetSlide('casos_clinicos_two_depo_2','','','

              <div class="text text-custon-2 wow animated fadeInUp">
                <p class="font-size-text">{{title}}</p>
                <p class="font-size-text" style="color: #f50f40; margin-top: -12px;">{{subtitle}}</p>
                <p class="font-size-text">{{text}}</p>
              </div>
                   

                ');
             ?>
          </div>
        </div>
        <!--end right box-->

      </div>
      <!--end item--> 
      
    </div>
  </div>
</section>
