<!--****************************************************** 2da sessao  *************************************************************-->


  
    <section class="sec-padding section-light" id="ROTA-RO">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom">
            <div class="carousel_holder">
              <div class="ce4-feature-box-41 wow animated fadeInLeft">
                <div id="sync1" class="owl-carousel">
                  <?php
                    echo funGetSlide('reabilitacao_two_imagem_grande','','','
                      <div class="item"><img src="{{img}}" alt="" class="img-responsive"></div>
                    ');
                  ?>
                  
                </div>
              </div>
              <br/>
              <div class="ce4-feature-box-42 wow animated fadeInLeft">
                <div id="sync2" class="owl-carousel">
                  <?php
                    echo funGetSlide('reabilitacao_two_imagem_pequena','','','
                      <div class="item"><img src="{{img}}" alt="" class="img-responsive"/></div>
                    ');
                  ?>
                  
                </div>
              </div>
            </div>
          </div>
          <!--end item-->
          
          <div class="col-md-6 col-sm-12 col-xs-12 margin-bottom padding-left-3">
            <?php
              echo funGetSlide('reabilitacao_two','','','

                <div class="col-xs-12 nopadding">
                  <div class="sec-title-container less-padding-3 text-left wow animated fadeInUp">
                    <div class="ce4-title-line-1 align-left" style="background-color: #f50f40;"></div>
                    <h4 class="uppercase font-weight-7 less-mar-1 font-size-title">{{title}}</h4>
                  </div>
                </div>
                <div class="clearfix"></div>
                <!--end title-->
                
                <h6 class="text-left raleway wow animated fadeInUp">{{subtitle}}</h6>
                <p class="text-left font-size-text wow animated fadeInUp">{{text}}</p>
                <div class="clearfix"></div>
                <br/>


              ');
            ?>
            
            <?php
              echo funGetSlide('reabilitacao_two_2','','','

                <div class="iconlist-2 wow animated fadeInUp" data-wow-delay="{{ctaTitle}}">
                  <p class="title-p">{{title}}</p>
                  <div class="icon dark"><i class="fa fa-arrow-circle-right" style="color: #f50f40;"></i></div>
                  <div class="text font-size-text">{{text}}</div>
                </div>

              ');
            ?>
            
            <div class="clearfix"></div>
            <br/>
            <br/>
            <a class="btn-2 btn-red btn-2-border uppercase smooth-scroll" href="#avaliacao">Marcar avaliação</a> </div>
          <!--end item--> 
          
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->