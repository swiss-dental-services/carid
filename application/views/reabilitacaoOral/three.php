
    <!--****************************************************** 3ra sessao  ******************************************************************-->        

    <section id="TID-RO">
      <div class="divider-line solid light"></div>
      <div class="container">
        <div class="row sec-padding">
          <?php
            echo funGetAdvancedBanners('reabilitacao_three', '

              
              <div class="col-md-6 col-sm-6 col-xs-12 margin-bottom">
                <div class="col-xs-12 nopadding wow animated fadeInUp">
                  <div class="sec-title-container less-padding-3 text-left">
                    <h3 class="uppercase font-weight-5 less-mar-1">{{title}}</h3>
                    <!--<div class="ce4-title-line-1 align-left"></div>-->
                  </div>
                </div>
                <div class="clearfix"></div>

                <h6 class="raleway wow animated fadeInUp">{{subtext}}</h6>
                <br/>
                <p class="font-size-text wow animated fadeInUp">{{text}} </p>
                <p class="font-size-text wow animated fadeInUp">{{subtitle}}</p>
                
                
                <div class="clearfix"></div>
                <br/>
                <br/>
                <a class="btn-2 btn-red btn-2-border uppercase smooth-scroll" href="{{callAction}}">{{callTitle}}</a> 

              </div>
              <!--end item-->
              
              <div class="col-md-6 col-sm-6 col-xs-12 OnlyDesktop wow animated fadeInRight"> <img src="{{img}}" alt="" class="img-responsive"/></div>
              <!--end item--> 

            ');
          ?>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    <!-- end section -->