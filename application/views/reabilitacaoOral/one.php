<!--****************************************************** 1ra sessao  **********************************************************************-->


  <section class="sec-padding">
    <div class="container">
      <?php
        echo funGetAdvancedBanners('reabilitacao_one', '

        <div class="row">
          <div class="col-xs-12 nopadding wow animated fadeInUp" data-wow-delay="0.8s">
            <div class="sec-title-container text-center">
              <div class="ce4-title-line-1" style="background-color: #f50f40;"></div>
              <h4 class="uppercase font-weight-7 less-mar-1 font-size-title">{{title}}</h4>
              <div class="clearfix"></div>
              <p class="by-sub-title font-size-text">{{subtitle}}</p>
            </div>
          </div>
          <div class="clearfix"></div>
          <!--end title-->
          
          <div class="col-md-9 col-centered less-padding-3 text-center">
            <h5 class="raleway wow animated fadeInUp" data-wow-delay="0.9s"><span class="text-gyellow nopadding" style="color: #f50f40;">{{text}}</span></h5>
            <p class="font-size-text wow animated fadeInUp" data-wow-delay="1.1s">{{subtext}}</p>
            <div class="clearfix"></div>
            <br/>
            <br/>
            <a class="btn-2 btn-red btn-2-border uppercase smooth-scroll" href="{{callAction}}">{{callTitle}}</a> </div>
          <!--end item--> 
        </div>
                                                  
          ');
      ?>
    </div>
  </section>
  <div class="clearfix"></div>
  <!-- end section -->












        
    