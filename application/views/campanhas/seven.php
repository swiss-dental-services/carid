
    <!--****************************************************** 7ma sessao  EQUIPE MÉDICA***********************************************************************-->

        <div class="page-section p-80-cont p-80-cont-mobil">
        
          <div class="container">
            <div class="row">

              <div class="col-md-8 fes9-img-cont clearfix desktopOnly" style="transform: translate(-138px, -99px);">
                <div class="fes9-img-center clearfix">
                  <div class="fes9-img-center">
                    <img src="<?php base_url('img-home-equipemedica-parte-1.png','img') ?>" alt="img" class="wow fadeInUp" data-wow-delay="150ms" data-wow-duration="1s" style="margin-top: 20%;  width: 136%; max-width: 225%;">
                    <img src="<?php base_url('img-home-equipemedica-parte-2.png','img') ?>" alt="img" class=" wow fadeInUp" data-wow-delay="450ms" data-wow-duration="1s" style="margin-top: 20%;  width: 136%; max-width: 225%;">
                  </div>
                </div>
              </div>


              <div class="col-md-4 vertical">
                <div class="mt-60 mb-80">
                  <h2 class="section-title red-padrao uppercase"><?= home_seven_text_1 ?><br><span class="bold"><?= home_seven_text_2 ?></span></h2>
                </div>

                <div class="row">

                  <?php
                      echo funGetAdvancedBanners('home_seven', '

                    <p class="pl-5">
                     {{subtitle}}
                    </p>

                    <p class="pl-5">
                     {{text}}
                    </p>');
                  ?>
                                       
                          
                </div>                        
                           
              </div>
            
            
            </div>

            <div class="row mt-100 mt-50-mobil mb-40-mobil" style="display: flex; justify-content: center;">                    
                <a class="button medium cta-franqueado gray-light shop-add-btn btn-obalo uppercase width-mobil" href="nossa-equipe">Conheça</a>
            </div> 

          </div>
        </div>