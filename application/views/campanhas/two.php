<!--****************************************************** 2da sessao  *************************************************************-->


  <section class="sec-tpadding-2 mb-section-mobil">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 nopadding">
        <div class="ce4-feature-box-1- ce4-feature-box-1-custon-1 margin-bottom" style="/*height: 842px;*/">
           <div class="img-box-main"> 
             <div class="img-box">
               <img src="<?php base_url('imagem_2_campanhas.jpg','img/custon') ?>" alt="" class="img-responsive"/>               
             </div>

             <div class="clearfix"></div>
             <br/>
             <div class="text text-custon-3 wow animated fadeInUp">
                <p class="font-size-text"><?=campanhas_two_text_1?></p>
             </div>           

           </div>

          </div>
      </div>
      <!--end item-->
      
      <div class="col-md-6 margin-bottom text-center nopadding">
      
      
        <div class="ce4-feature-box-48 height-custon" style="/*height: 711px;*/">
        
          <div class="text-box-main">
            <div class="text-box">
              <div class="col-xs-12 nopadding">
                <div class="sec-title-container text-center wow animated fadeInUp">
                  <br/><br/>
                  <h4 class="ce4-big-title uppercase font-weight-b mobil-title-1"><?=campanhas_two_text_2?> <br><?=campanhas_two_text_3?> <br><?=campanhas_two_text_4?></h4>
                  <!--<h6>Since - 1964</h6>-->
                </div>
              </div>
              <div class="clearfix"></div>
              <!--end title-->     
              <?php
                echo funGetSlide('campanhas_two','','','
                  
                <h4 class="raleway wow animated fadeInUp" data-wow-delay="0.0s">{{subtitle}} </h4>
                <br/>
                <p class="font-size-text" class="wow animated fadeInUp" data-wow-delay="0.2s">{{text}}</p>
                ');
              ?>                       
                 
              <div class="clearfix"></div>

              <!--<a href="#" class="ce4-readmore-big">Read our all services</a>-->

            </div>
          </div>
        </div>
        <!--end right box-->

      </div>
      <!--end item--> 
      
    </div>
  </div>
</section>


