<!--****************************************************** 9na sessao seja um franqueado************************************************************************-->      

<div class="page-section " style="margin-top: -1px">
    <div class="work-proc-1-bg-3" >
        <div class="container fes4-cont-2">
            <div class="row">
                
                <div class="col-md-6 left-desktop">
                    <div class="fes2-main-text-cont fes2-main-text-cont-mobil">
                        <div class="title-fs-45 uppercase">
                            <?= home_nine_text_1 ?><br>
                            <span class="bold uppercase"><?= home_nine_text_2 ?></span>
                        </div>
                        <div class="line-3-100-"></div>

                        <?php
                            echo funGetAdvancedBanners('home_nine', '

                        <div class="fes2-text-cont">
                          <p class="grey-ligth">{{text}}</p>

                          <br>

                          <p class="grey-dark ft-600">{{subtext}}</p>  
                        </div>
                        ');

                        ?>
                        <div class="mt-30">
                          <a class="button medium cta-franqueado shop-add-btn btn-obalo uppercase w-70-2" href="javascript: void(0)" style="">quero ser um <span class="ft-700">franqueado</span></a>
                        </div>
                    </div>
                </div>                
  
            </div>
        </div>
    </div>        
</div>