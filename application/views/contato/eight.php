<!--****************************************************** 8va sessao  clinicas e franquias********************************************************************-->


        <!-- PORTFOLIO SECTION 3 CAR-->
        <div class="page-section pt-50-mobil">
          <div class="relative">
          
            
            <ul class="port-grid clearfix" id="items-grid">

              
              <li class="port-item port-item-width-2 mix">
                <div class="port-text-cont relative">
                  <img class="port-main-img" src="<?php base_url('car-bg-wide.png','img') ?>" alt="img" >
                    <div class="block-center-xy">
                      <div class="title-fs-45-wide uppercase mb-30 ml-40">
                        <?= home_eight_text_1 ?><br>
                        <span class="bold"><?/*=home_eight_text_2*/ ?></span>
                      </div>
                      <div class="desktopOnly">

                        <?php
                            echo funGetAdvancedBanners('home_eight', '

                        <p>{{text}}</p>

                        <p>{{subtext}}</p>');

                        ?>

                      </div>
                    </div>

                </div>
              </li>   
              
              
              <li class="port-item mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-1-braga.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">SÃO PAULO</a></h3>
                      <span style="color: #fff; font-weight: 200;">São Paulo</span>
                    </div>
                    
                    <div class="port-btn-cont">
                      <a href="<?php echo isMobile() ? base_url('mobile-sao-paulo.png','img') : base_url('info-sao-paulo.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="javascript: void(0)" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>

              
              <li class="port-item mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-2-santarem.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">CURITIBA</a></h3>
                      <span style="color: #fff; font-weight: 200;">Paraná</span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php echo isMobile() ? base_url('mobile-curitiba.png','img') : base_url('info-curitiba.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="javascript: void(0)" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
              
              
             <!-- <li class="port-item mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-3-coimbra.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">COIMBRA</a></h3>
                      <span style="color: #fff; font-weight: 200;">Portugal</span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('unidades.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com/maps/place/Swiss+Dental+Services/@40.184503,-8.405196,16z/data=!4m5!3m4!1s0x0:0x523dc28566003ee4!8m2!3d40.1845033!4d-8.4051965?hl=pt-PT" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
              
              
              <li class="port-item mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-4-viseu.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">VISEU</a></h3>
                      <span style="color: #fff; font-weight: 200;">Portugal</span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php base_url('img-home-clinicas-4-viseu.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="https://www.google.com/maps?ll=40.649775,-7.913444&z=16&t=m&hl=pt-PT&gl=BR&mapclient=embed&cid=14760922786242283040" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li> -->

              <li class="port-item port-item-width-2 mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-florianópolis.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">FLORIANÓPOLIS</a></h3>
                      <span style="color: #fff; font-weight: 200;">Santa Catarina</span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php echo isMobile() ? base_url('mobile-florianopolis.png','img') : base_url('info-florianopolis.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="javascript: void(0)" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
              
              
              <li class="port-item port-item-width-2 mix">
                <a href="javascript: void(0)">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php base_url('img-home-clinicas-5-portimao.png','img') ?>" alt="img" >
                  </div>
                </a>
                <div class="port-overlay-cont">

                    <div class="port-title-cont">
                      <h3><a href="javascript: void(0)">PORTO ALEGRE</a></h3>
                      <span style="color: #fff; font-weight: 200;">Rio grande do sul</span>
                    </div>
                    <div class="port-btn-cont">
                      <a href="<?php echo isMobile() ? base_url('mobile-porto-alegre.png','img') : base_url('info-porto-alegre.png','img') ?>" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search" title="Informação" ></div></a>
                      <a href="javascript: void(0)" target="" style="font-size: 22px"><div aria-hidden="true" class="icon icon-basic-geolocalize-05" title="Geolocalização" style="transform: translateY(3px);"></div></a>
                    </div>

                </div>
              </li>
              

            </ul>
          
          </div>
        </div> 