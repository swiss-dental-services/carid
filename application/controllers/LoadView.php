<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LoadView extends CI_Controller {
	
	public function index() {

		$this->data['active']          		  = 'home';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE UMA';
		$this->data['title_formulario_2']     = ' CONSULTA';

		$this->load->view('index', $this->data);
		
	}

	public function construcao() {		

		$this->load->view('construcao');
		
	}

	public function sobre() {

		$this->data['active']          		  = 'so';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('sobre', $this->data);
		
	}


	public function condicoesFinanciamento() {

		$this->data['active']          = 'cf';
        $this->data['active_submenu']  = '';

        
		$this->load->view('condicoesFinanciamento', $this->data);
		
	}

	public function reabilitacaoOral() {

		$this->data['active']          		  = 'ro';
		$this->data['active_submenu']  		  = '';
		
		
		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('reabilitacaoOral', $this->data);
		
	}

	public function casosClinicos() {

		$this->data['active']          		  = 'cc';
		$this->data['active_submenu']  		  = '';
		

		
		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';
		
		$this->load->view('casosClinicos', $this->data);
		
	}

	public function campanhas() {

		$this->data['active']          		  = 'camp';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('campanhas', $this->data);
		
	}

	public function contacto() {

		$this->data['active']          		  = 'contato';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('contato', $this->data);
		
	}

	public function nossaEquipe() {

		$this->data['active']          		  = 'nossa-equipe';
		$this->data['active_submenu']  		  = '';
		

		$this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('nossaEquipe', $this->data);
		
	}

	public function suporte() {

    	$this->data['active']          		  = 'sfc';
        $this->data['active_submenu']  		  = 'suport';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('suporte', $this->data);
		
	}

	public function trabalheConosco() {

		$this->data['active']          		  = 'sfc';
        $this->data['active_submenu']  		  = 'trab-con';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('trabalheConosco', $this->data);
		
	}

	public function marcas() {

		$this->data['active']          		  = 'mark';
        $this->data['active_submenu']  		  = '';

        $this->data['title_formulario_1']     = 'MARQUE A SUA';
		$this->data['title_formulario_2']     = ' AVALIAÇÃO!';

		$this->load->view('marcas', $this->data);
		
	}

	
	public function blog() {

		$this->data['active']          = 'blog';
        $this->data['active_submenu']  = '';

        $this->data['active_trat']     = '';
        $this->data['active_serv']     = '';

		$this->load->view('blog', $this->data);
		
	}

	public function blogDetail($cat,$post) {


		$this->data['active']          = 'blog';
        $this->data['active_submenu']  = '';
        $this->data['post_id']  = url_to_id($post);

        $this->data['active_trat']     = '';
        $this->data['active_serv']     = '';

		$this->load->view('blogDetail', $this->data);
		
	}

	
    public function termosCondicoes() {

    	$this->data['active']          = '';
		$this->data['active_submenu']  = '';

		$this->data['active_trat']     = '';
        $this->data['active_serv']     = '';
		
		$this->load->view('termosCondicoes', $this->data);
		
	}

	public function obrigado() {

    	$this->data['active']          = '';
		$this->data['active_submenu']  = '';

		$this->data['active_trat']     = '';
        $this->data['active_serv']     = '';
		
		$this->load->view('obrigado', $this->data);
		
	}
	

}
