  let data_arrays = []  
  //let message = ""
  //let end_message = ""
  $(document).ready(function() {
      $('#FORM-NEWSLETTER-CARID').on('submit', function(ev){
        ev.preventDefault();

        //alert("aprestaste el boton");
                
        var nome = $('#FORM-NEWSLETTER-CARID').find('input[name=name]').val();
        var email = $('#FORM-NEWSLETTER-CARID').find('input[name=email]').val();
        
        
        var origem = "site-CARID";
        var tk_rdstation  = '1bdf5aee9caf657d422422327ad6bdd3';
        var identificador = 'FORM-NEWSLETTER-CARID';
        var _is = '6';
        var client_id = '';
        var perfil = '';
        

        data_arrays = {
          'token_rdstation': tk_rdstation ,
          'id': identificador ,
          '_is': _is ,
          'client_id': client_id  ,
          'perfil': perfil  ,          
          'name': nome  ,          
          'email': email  ,          
          'origem': origem ,
          'last_page': document.referrer
        };        
        
        send();
        
      })
  
  });
  
  function str_cookie(str) {
    str = str.split('; ');
    var result = {};
    for (var i = 0; i < str.length; i++) {
        var cur = str[i].split('=');
        result[cur[0]] = cur[1];
    }
    return result;
  }
  
  function checkLead(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        callback(this.response);
      } else if (this.status == 404) {
        callback("FALSE||");
      } else if (this.status == 500) {
        callback("FALSE||");
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send(); 
    xhttp.onreadystatechange();
  }
  
  function compareDate(response) {
    responseArray = response.split("||");
    // 
    if (typeof responseArray[0] !== 'undefined') {
      //TRATAMENTO CASO NÃƒO RECEBA TRUE, ou seja nÃ£o encontrou a pessoa procurada ou teve falha no arquivo.
      if (responseArray[0] != "TRUE") {
        send(); 
      // CASO A PESSOA SEJA ENCONTRADA...
      } else {
        leadDate = new Date(responseArray[1].replace(/"/g, "").split(" ")[0]);
        dias = compareDays(leadDate, today());
        // SE ESTA NA BASE DE DADOS A MAIS DE 90 DIAS, ENVIA O FORULÃRIO
        if (dias > 90) {
          send()
        // CASO TENHA MENOS DE 90 DIAS ENTRA NAS QUESTÃ•ES
        } else {
          var message = `Reparamos que temos uma inscrição sua a ${dias} dias, tem a certeza que pretende se inscrever novamente?`;
          if (dias == 0) {
            message = "Reparamos que temos uma inscrição recente sua, tem a certeza que pretende se inscrever novamente?";
          }
          var r = confirm(message);
          // CASO TENHA CONFIRMADO, ENVIA COM PRIORIDADE
          if (r) {
            //data_arrays.push({ name: 'prioridade', value: 'sim' })
            send()
            // CASO NÃƒO CONFIRME, NÃƒO ENVIA O FORMULÃRIO.
          } else {
            //alert("Obrigado pela sua atenção")
            window.location.href = "thank-page";
          }
        }
      }
    // CASO QUALQUER PROBLEMA OCORRA NA VALIDAÃ‡ÃƒO SEM TER TIDO TRATAMENTO, ENVIA O FORMULÃRIO.
    } else {
      send()
    }
  }
  function send() {
    setCookie("nameUser", $('#FORM-NEWSLETTER-CARID').find('input[name=name]').val() ,30);
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() { // Chama a funÃ§Ã£o quando o estado mudar.
        if (this.readyState == 4 && this.status == 200) {
          //alert("Obrigado pela sua inscrição!")
          /*window.location.href = "thank-page";*/
          //window.location = window.location;

          $("#btn-1").prop("disabled", true);
          $('html, body').css("cursor", "wait");

          Swal.fire({
            type: "success",
            title: 'Obrigado pelo seu interesse.',
            html: '<p>O seu registo foi <strong> submetido com sucesso</strong>. Brevemente será contactado pela nossa equipe.</p>',
            onClose: function(){

              $('#name').val("");
              $('#email').val("");
              $('#btn-1').prop("disabled", false);
              $('html, body').css("cursor", "default");

            }
          });

        }
    }
    xhr.open("POST", 'https://gest.swissdentalservices.com/leads-api/import-leads.php', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded", "Access-Control-Allow-Origin: *");
    xhr.send("data="+JSON.stringify(data_arrays));
    console.log(data_arrays);
  }
  function today() {
    tempDate = new Date();
    stringDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() +1) + '-' + tempDate.getDate();
    return new Date(stringDate);
  }
  function compareDays(firstDate, secondDate) {
    var oneDay = 24 * 60 * 60 * 1000; 
    return Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)))
  }
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }